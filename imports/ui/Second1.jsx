import {Link} from 'react-router'
import ReactDOM from 'react-dom';
import React, { Component, PropTypes } from 'react';

import { Tasks8 } from '../api/tasks8.js';
import C3V from './C3V'
import Cv2 from './Cv2'
import { createContainer } from 'meteor/react-meteor-data';
class Second1 extends Component{

    renderTasks(){
        const huzaifa2=this.props.huzaifa;

        return this.props.tasks.map((task2) => (
            <C3V key={task2._id} task={task2} huzaifa={huzaifa2} />
        ));

    }


    render() {
        return (
            <div className="sixth container">
                {this.renderTasks()}
            </div>
        );
    }}


Second1.propTypes = {
    tasks: PropTypes.array.isRequired,
};

export default createContainer(() => {
    return {
        tasks: Tasks8.find({}, { sort: { createdAt: -1 } }).fetch(),


    };
}, Second1);