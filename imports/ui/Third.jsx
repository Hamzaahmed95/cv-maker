import {Link} from 'react-router'
import Cv2 from './Cv2'
import React, { Component, PropTypes } from 'react';

import { Tasks } from '../api/tasks.js';
import { createContainer } from 'meteor/react-meteor-data';
class Third extends Component{
    renderTasks2(){
        return this.props.tasks.map((task) => (
            <Cv2 key={task._id} task={task} />
        ));

    }


    render() {
        return (
            <div className="container">
                {this.renderTasks2()}
            </div>
        );
    }}


Third.propTypes = {
    tasks: PropTypes.array.isRequired,
   };

export default createContainer(() => {
    return {
        tasks: Tasks.find({}, { sort: { createdAt: -1 } }).fetch(),

    };
}, Third);