import {Link} from 'react-router'
import App from './App'
import { Tasks } from '../api/tasks.js';
import React, { Component, PropTypes } from 'react';

import { createContainer } from 'meteor/react-meteor-data';



class Cv2 extends  Component{
    render() {
        return (
            <div>
                <div className="wrapper2">

                    <aside2>
                        <section2 className="popular-recipes">
                            <img className="img-circle2" src="/images/dp.jpg"/>
                            <h2>Profile Summary</h2>
                            <a2 href="">Motivation</a2>
                            <a2 href="">Technical Skilss</a2>
                            <a2 href="">Strength</a2>
                            <a2 href="">Extra-Curricular</a2>
                            <a2 href="">Electives</a2>
                            <a2 href="">Experience</a2>
                            <a2 href="">Achievements</a2>
                        </section2>
                        <div className="center12">
                            <h2>HTML</h2>
                            <hr2 className="hr12" width="30px"/>
                            <h2>CSS</h2>
                            <hr2 className="hr12"/>
                            <h2>Java Script</h2>
                            <hr2 className="hr12"/>
                            <h2>Jquery</h2>
                            <hr2 className="hr12"/>
                            <h2>MeteorJs</h2>
                            <hr2 className="hr12"/>
                            <h2>AngularJs</h2>
                            <hr2 className="hr12"/>
                            <h2>Mongodb</h2>
                            <hr2 className="hr12"/>
                            <h2>Reactjs</h2>
                            <hr2 className="hr12"/>

                        </div>
                        <section2 className="contact-details2">
                            <h2>Contact</h2>
                            <p>Email : {this.props.task.email}<br /></p>
                            <p>Address :<br />
                                {this.props.task.address},Karachi<br />
                                Pakistan</p>
                        </section2>
                    </aside2>
                    <div className="hello2">
                        <h1 className="heading12"><cite>{this.props.task.name}</cite></h1>

                        <br/><h4 className="heading22">Contact detail:{this.props.task.contact}  /  {this.props.task.email}</h4>
                    </div>
                    <section2 className="courses2">
                        <article2>
                            <div className="para12">
                                <h3>Motivation</h3>
                                <p>{this.props.task.motiv}</p></div>
                        </article2>
                        <article2>
                            <div className="para12">
                                <h3>Technical Skills</h3>
                                <p>{this.props.task.technicalskills}</p></div>
                        </article2><article2>
                        <div className="para12">
                            <h3>Strength</h3>
                            <ul>
                                <li>{this.props.task.Strengths}</li>

                            </ul></div>
                    </article2>
                        <article2>
                            <div className="para12">
                                <h3>Electives:</h3>
                                <ul>
                                    <li>{this.props.task.elective}
                                    </li>

                                </ul></div>
                        </article2>
                        <article2>
                            <div className="para12">
                                <h3>Hobby</h3>
                                <ul>
                                    <li>{this.props.task.hobby}
                                    </li>

                                </ul></div>
                        </article2>
                        <article2>
                            <div className="para12">
                                <h3>Achievements:</h3>
                                <ul>
                                    <li>{this.props.task.achievments}
                                    </li>

                                </ul></div>
                        </article2>

                    </section2>

                    <footer2>
                        &copy; 2017 {this.props.task.name}
                    </footer2>
                </div></div>
        );
    }}

export default  Cv2 ;