import {Link} from 'react-router'
import ReactDOM from 'react-dom';
import React, { Component, PropTypes } from 'react';

import { Tasks14 } from '../api/tasks14.js';
import Cv4 from './Cv4'
import C14 from './C14'
import { createContainer } from 'meteor/react-meteor-data';
class Second2 extends Component{

    renderTasks(){

        const huzaifa2=this.props.huzaifa;
        return this.props.tasks.map((task) => (

            <C14 key={task._id} task={task} huzaifa={huzaifa2}/>

        ));

    }


    render() {
        return (
            <div className="sixth container">
                {/*console.log("in render"+this.props.huzaifa)*/}
                {/*<img src={this.props.huzaifa}/>*/}
                {this.renderTasks()}

            </div>
        );
    }}


Second2.propTypes = {
    huzaifa:PropTypes.string.isRequired,
};

export default createContainer(() => {
    return {
        tasks: Tasks14.find({}).fetch(),

    };
}, Second2);