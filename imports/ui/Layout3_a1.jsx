import React, {Component,PropTypes} from 'react';
import {Meteor} from 'meteor/meteor'
import { createContainer } from 'meteor/react-meteor-data'
import Task from './Task.jsx';
import { Tasks19 } from '../api/tasks19.js';
import Cv from './Cv'
import Cv2 from './Cv2'
import Home1 from './Home1'
import {Link} from 'react-router'
import ReactDOM from 'react-dom';
import Layout31 from './Layout31'
class Layout3_a1 extends Component{


    setTimeout(){
        this.setTimeout(this, 5000)
    }



    handleSubmit(event){

        // const url=huzaifa;
        //console.log ("url = "+url);

        event.preventDefault;
        // Find the text field via the React ref

        const name = ReactDOM.findDOMNode(this.refs.contact_name).value.trim();
        const Dev = ReactDOM.findDOMNode(this.refs.dev).value.trim();
        const Dob = ReactDOM.findDOMNode(this.refs.birth_date).value.trim();
        const contact = ReactDOM.findDOMNode(this.refs.contact_detail).value.trim();
        const email = ReactDOM.findDOMNode(this.refs.email).value.trim();
        const address = ReactDOM.findDOMNode(this.refs.address).value.trim();
        const motiv = ReactDOM.findDOMNode(this.refs.motivation).value.trim();
        const technicalskills = ReactDOM.findDOMNode(this.refs.skills).value.trim();
        const technicalskills2 = ReactDOM.findDOMNode(this.refs.skills2).value.trim();
        const desc1 = ReactDOM.findDOMNode(this.refs.desc1).value.trim();
        const desc2 = ReactDOM.findDOMNode(this.refs.desc2).value.trim();
        const college_name = ReactDOM.findDOMNode(this.refs.college_name).value.trim();
        const college_major = ReactDOM.findDOMNode(this.refs.college_major).value.trim();
        const from_college = ReactDOM.findDOMNode(this.refs.from_college).value.trim();
        const to_college = ReactDOM.findDOMNode(this.refs.to_college).value.trim();
        const university_name = ReactDOM.findDOMNode(this.refs.university_name).value.trim();
        const university_major = ReactDOM.findDOMNode(this.refs.university_major).value.trim();
        const from_university = ReactDOM.findDOMNode(this.refs.from_university).value.trim();
        const to_university = ReactDOM.findDOMNode(this.refs.to_university).value.trim();
        const cname1 = ReactDOM.findDOMNode(this.refs.cname1).value.trim();
        const post1 = ReactDOM.findDOMNode(this.refs.post1).value.trim();
        const descr1 = ReactDOM.findDOMNode(this.refs.descr1).value.trim();
        const cname2 = ReactDOM.findDOMNode(this.refs.cname2).value.trim();
        const post2 = ReactDOM.findDOMNode(this.refs.post2).value.trim();
        const descr2 = ReactDOM.findDOMNode(this.refs.descr2).value.trim();
        const cname1_to = ReactDOM.findDOMNode(this.refs.cname1_to).value.trim();
        const cname1_from = ReactDOM.findDOMNode(this.refs.cname1_from).value.trim();
        const cname2_to = ReactDOM.findDOMNode(this.refs.cname2_to).value.trim();
        const cname2_from = ReactDOM.findDOMNode(this.refs.cname2_from).value.trim();


        const languages = ReactDOM.findDOMNode(this.refs.languages).value.trim();
        const image = ReactDOM.findDOMNode(this.refs.image).value.trim();

        Tasks19.insert({
            name,
            Dev,
            Dob,
            contact,
            email,
            address,
            motiv,
            technicalskills,
            desc1,
            technicalskills2,
            desc2,
            cname1_to,
            cname1_from,
            cname2_to,
            cname2_from,
            cname1,
            post1,
            descr1,
            cname2,
            post2,
            descr2,
            college_name,
            college_major,
            from_college,
            to_college,
            university_name,
            university_major,
            from_university,
            to_university,
            languages,

            image,
            //url,

            createdAt: new Date(), // current time
        });
        // Clear form
        ReactDOM.findDOMNode(this.refs.contact_name).value="";
        ReactDOM.findDOMNode(this.refs.dev).value="";
        ReactDOM.findDOMNode(this.refs.contact_detail).value="";
        ReactDOM.findDOMNode(this.refs.email).value="";
        ReactDOM.findDOMNode(this.refs.address).value="";
        ReactDOM.findDOMNode(this.refs.motivation).value="";
        ReactDOM.findDOMNode(this.refs.skills).value="";

        ReactDOM.findDOMNode(this.refs.skills2).value="";
        ReactDOM.findDOMNode(this.refs.cname1).value="";
        ReactDOM.findDOMNode(this.refs.post1).value="";
        ReactDOM.findDOMNode(this.refs.descr1).value="";
        ReactDOM.findDOMNode(this.refs.cname2).value="";
        ReactDOM.findDOMNode(this.refs.post2).value="";
        ReactDOM.findDOMNode(this.refs.descr2).value="";
        ReactDOM.findDOMNode(this.refs.cname1_to).value="";
        ReactDOM.findDOMNode(this.refs.cname1_from).value="";
        ReactDOM.findDOMNode(this.refs.cname2_to).value="";
        ReactDOM.findDOMNode(this.refs.cname2_from).value="";
        ReactDOM.findDOMNode(this.refs.desc1).value="";
        ReactDOM.findDOMNode(this.refs.desc2).value="";
        ReactDOM.findDOMNode(this.refs.college_name).value="";
        ReactDOM.findDOMNode(this.refs.college_major).value="";
        ReactDOM.findDOMNode(this.refs.from_college).value="";
        ReactDOM.findDOMNode(this.refs.to_college).value="";
        ReactDOM.findDOMNode(this.refs.university_name).value="";
        ReactDOM.findDOMNode(this.refs.university_major).value="";
        ReactDOM.findDOMNode(this.refs.from_university).value="";
        ReactDOM.findDOMNode(this.refs.to_university).value="";
        ReactDOM.findDOMNode(this.refs.languages).value="";
        ReactDOM.findDOMNode(this.refs.image).value="";
    }

    _handleImageChange(e) {
        e.preventDefault();

        let reader = new FileReader();
        let file = e.target.files[0];

        reader.onloadend = () => {
            this.setState({
                file: file,
                imagePreviewUrl: reader.result
            });
        }

        reader.readAsDataURL(file)
    }
    constructor(props) {
        super(props);
        this.state = {
            file: '',
            imagePreviewUrl: '',
            error:'',
            error2:'',
            error4:'',
            error5:'',
            name:'',
            Dev:'',
            Dob:'',
            contact:'',
            email:'',
            address:'',
            motiv:'',
            technicalskills:'',
            Desc1:'',
            technicalskills2:'',
            Desc2:'',
            Cname1:'',
            post1:'',
            Descr1:'',
            Cname2:'',
            post2:'',
            Descr2:'',

            College_name:'',
            cmajor:'',
            cFrom:'',
            cTo:'',
            Uni_name:'',
            umajor:'',
            uFrom:'',
            uTo:'',
            language:'',
        };
        this._handleImageChange = this._handleImageChange.bind(this);
        this._handleSubmit = this._handleSubmit.bind(this);
        this.handleChangename = this.handleChangename.bind(this);
        this.handleChangeDev = this.handleChangeDev.bind(this);
        this.handleDob = this.handleDob.bind(this);
        this.handleChangecontact = this.handleChangecontact.bind(this);
        this.handleChangeemail = this.handleChangeemail.bind(this);
        this.handleChangeaddress = this.handleChangeaddress.bind(this);
        this.handleChangemotiv = this.handleChangemotiv.bind(this);
        this.handleChangetechnicalskills = this.handleChangetechnicalskills.bind(this);
        this.handleDesc1 = this.handleDesc1.bind(this);
        this.handleChangetechnicalskills2 = this.handleChangetechnicalskills2.bind(this);
        this.handleDesc2 = this.handleDesc2.bind(this);
        this.handleCollege_name = this.handleCollege_name.bind(this);
        this.handlecmajor = this.handlecmajor.bind(this);
        this.handlecFrom = this.handlecFrom.bind(this);
        this.handlecTo = this.handlecTo.bind(this);
        this.handleUni_name = this.handleUni_name.bind(this);
        this.handleumajor = this.handleumajor.bind(this);
        this.handleuFrom = this.handleuFrom.bind(this);
        this.handleuTo = this.handleuTo.bind(this);
        this.handleCname1 = this.handleCname1.bind(this);
        this.handlepost1 = this.handlepost1.bind(this);
        this.handleDescr1 = this.handleDescr1.bind(this);

        this.handleCname2 = this.handleCname2.bind(this);
        this.handlepost2 = this.handlepost2.bind(this);
        this.handleDescr2 = this.handleDescr2.bind(this);
        this.handleChangelanguage = this.handleChangelanguage.bind(this);

    }

    _handleSubmit(e) {
        //  e.preventDefault();
        //this.state.imagePreviewUrl = image
        // TODO: do something with -> this.state.file

    }


    renderTasks19() {
        return this.props.tasks.map((task) => (
            <Cv key={task._id} task={task}/>
        ));
    }



    renderTasks192() {
        return this.props.tasks.map((task) => (
            <Cv2 key={task._id} task={task}/>
        ));
    }
    showIt() {
        document.getElementsByClassName("div1").style.visibility = "visible";
        setTimeout("showIt()", 1000); // after 1 sec
    }
    handleChangename(e){
        console.log(e.target.value.match("^[a-zA-Z ]*$"));
        if(e.target.value.match("^[a-zA-Z ]*$")!=null)
        {
            this.setState({name: e.target.value});
            this.setState({error:""});
        }
        else
        {this.setState({error:"Only alphabets allowed!"});

            this.setState({name:''});
            console.log("error"+this.state.error);}

    }
    handleCname1(e){
        this.setState({Cname1: e.target.value});
        console.log("Cname1 here: "+this.state.Cname1);
    }
    handlepost1(e){
        this.setState({post1: e.target.value});
        console.log("post1 here: "+this.state.post1);
    }
    handleDescr1(e){
        this.setState({Descr1: e.target.value});
        console.log("Descr1 here: "+this.state.Descr1);
    }
    handleCname2(e){
        this.setState({Cname2: e.target.value});
        console.log("Cname2 here: "+this.state.Cname2);
    }
    handlepost2(e){
        this.setState({post2: e.target.value});
        console.log("post2 here: "+this.state.post2);
    }

    handleDescr2(e){
        this.setState({Descr2: e.target.value});
        console.log("Descr2 here: "+this.state.Descr2);
    }

    handleChangeDev(e){
        console.log(e.target.value.match("^[a-zA-Z ]*$"));
        if(e.target.value.match("^[a-zA-Z ]*$")!=null)
        {
            this.setState({Dev: e.target.value});
            this.setState({error5:""});
        }
        else
        {this.setState({error5:"Only alphabets allowed!"});

            this.setState({Dev:''});
            console.log("error5"+this.state.error5);}

    }
    handleDob(e){
        this.setState({Dob: e.target.value});
        console.log("Dob here"+this.state.Dob);

    }
    handleChangecontact(e){
        console.log(e.target.value.match("^[0-9]*$"));
        if(e.target.value.match("^[0-9]*$")!=null){
            this.setState({contact: e.target.value});
            this.setState({error2:""});
        }
        else
        {this.setState({error2:"Only numbers allowed!"});

            this.setState({contact:''});
            console.log("error2"+this.state.error2);}

    }
    handleChangeemail(e){
        console.log("here->"+e.target.value.match(/^[a-zA-Z0-9]+@[a-zA-Z0-9]+\.[A-Za-z]+$/));
        if(/^[A-Z0-9'.1234z_%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test(e.target.value)){
            this.setState({email: e.target.value});
            this.setState({error4:""});
        }
        else
        {this.setState({error4:"Invalid Email!"});
            this.setState({email: ''});
            console.log("error4"+this.state.error4);}

    }
    handleChangeaddress(e){
        this.setState({address: e.target.value});
        console.log("address"+this.state.address);
    }
    handleChangemotiv (e){
        this.setState({motiv: e.target.value});
        console.log("motiv"+this.state.motiv);
    }
    handleChangetechnicalskills(e){
        this.setState({technicalskills: e.target.value});
        console.log("technicalskills"+this.state.technicalskills);
    }
    handleDesc1(e){
        this.setState({Desc1: e.target.value});
        console.log("Desc1 here: "+this.state.Desc1);
    }
    handleChangetechnicalskills2(e){
        this.setState({technicalskills2: e.target.value});
        console.log("technicalskills"+this.state.technicalskills2);
    }
    handleDesc2(e){
        this.setState({Desc2: e.target.value});
        console.log("Desc2 here: "+this.state.Desc2);
    }

    handleCollege_name(e){
        this.setState({College_name: e.target.value});
        console.log("College_name here: "+this.state.College_name);
    }
    handlecmajor(e){
        this.setState({cmajor: e.target.value});
        console.log("cmajor here: "+this.state.cmajor);
    }
    handlecFrom(e){
        this.setState({cFrom: e.target.value});
        console.log("cFrom here: "+this.state.cFrom);
    }
    handlecTo(e){
        this.setState({cTo: e.target.value});
        console.log("cTo here: "+this.state.cTo);
    }
    handleUni_name(e){
        this.setState({Uni_name: e.target.value});
        console.log("Uni_name here: "+this.state.Uni_name);
    }
    handleumajor(e){
        this.setState({umajor: e.target.value});
        console.log("umajor here: "+this.state.umajor);
    }
    handleuFrom(e){
        this.setState({uFrom: e.target.value});
        console.log("uFrom here: "+this.state.uFrom);
    }
    handleuTo(e){
        this.setState({uTo: e.target.value});
        console.log("uTo here: "+this.state.uTo);
    }

    handleChangelanguage(e){
        this.setState({language: e.target.value});
        console.log("language"+this.state.language);
    }









    render()
    {
        let {imagePreviewUrl} = this.state;
        let $imagePreview = null;
        if (imagePreviewUrl) {

            $imagePreview = (<img src={imagePreviewUrl} />);

        }
        const huzaifa = imagePreviewUrl;
        return (
            <div className="hamza container">
                <div className="text-center">
                    <img src="images/cv.png" className="rounded" alt="..." height="10%" width="10%"/>
                </div>
                <div className="text-center">
                    <h4 className="text-center c2">CV MAKER</h4>
                </div>
                <div className="hamza21">

                    <div className="text-center">
                        <img className="rounded" width="45%" height="45%" src ="images/l2.png" />
                    </div>
                    <div className="center">

                        <div className="row hamza3row">
                            <div className="col-sm-6 text-center" >



                                <button type="button" className="btn btn-primary btn-lg " data-toggle="modal" data-target="#myModal">
                                    Information
                                </button></div>

                            <div className="col-sm-6 text-center">
                                <button type="button" className=" btn btn-primary btn-lg " data-toggle="modal" data-target="#myModal2">
                                    Show CV
                                </button></div></div>
                    </div>

                    <div className="modal fade" id="myModal" role="dialog">
                        <div className="modal-dialog">

                            <div className="modal-content">
                                <div className="modal-header">
                                    <button type="button" className="close" data-dismiss="modal">&times;</button>
                                    <h4 className="modal-title">Add your Information</h4>
                                </div>
                                <div className="modal-body modalC1">
                                    <form>
                                        <div className="form-group">
                                            <span className="error">*</span><label>Name: </label><span className="error">{this.state.error}</span>
                                            <input type="text" className="form-control" ref="contact_name" onChange={this.handleChangename}
                                                   placeholder="Enter Name " name="[required]"/>
                                        </div>
                                        <div className="form-group">
                                            <span className="error">*</span><label>Developer: </label><span className="error">{this.state.error5}</span>
                                            <input type="text" className="form-control" ref="dev" onChange={this.handleChangeDev}
                                                   placeholder="You are? " name="[required]"/>
                                        </div>
                                        <div className="form-group">
                                            <span className="error">*</span><label>Date of Birth</label>
                                            <input type="date" className="form-control" onChange={this.handleDob} id="exampleInputDOB1" placeholder="Date of Birth" ref="birth_date"/>
                                        </div>
                                        <div className="form-group">
                                            <span className="error">*</span><label>Contact No:</label><span className="error">{this.state.error2}</span>
                                            <input type="text" className="form-control" ref="contact_detail" onChange={this.handleChangecontact}
                                                   placeholder="Enter contact No"/>
                                        </div>
                                        <div className="form-group">
                                            <span className="error">*</span><label>Enter Email:</label><span className="error">{this.state.error4}</span>
                                            <input type="text" className="form-control" ref="email" onChange={this.handleChangeemail}
                                                   placeholder="Enter email"/>
                                        </div>
                                        <div className="form-group">
                                            <span className="error">*</span> <label>Enter Address:</label>
                                            <input type="text" className="form-control" ref="address" onChange={this.handleChangeaddress}
                                                   placeholder="Enter Address"/>
                                        </div>
                                        <div className="form-group">
                                            <span className="error">*</span> <label>Enter Motivation:</label>
                                            <input type="text" className="form-control" ref="motivation" onChange={this.handleChangemotiv}
                                                   placeholder="Enter your motivation"/>
                                        </div>
                                        <div className="form-group">
                                            <span className="error">*</span> <label>Technical Skills:</label>
                                            <input type="text" className="form-control" ref="skills" onChange={this.handleChangetechnicalskills}
                                                   placeholder="Your Technical skills"/>
                                        </div>
                                        <div className="form-group">
                                            <span className="error">*</span>   <label>Description:</label>
                                            <input type="text" className="form-control" ref="desc1" onChange={this.handleDesc1}
                                                   placeholder="Description"/>
                                        </div>
                                        <div className="form-group">
                                            <span className="error">*</span> <label>Technical Skills 2:</label>
                                            <input type="text" className="form-control" ref="skills2" onChange={this.handleChangetechnicalskills2}
                                                   placeholder="Your Technical skills"/>
                                        </div>
                                        <div className="form-group">
                                            <span className="error">*</span>   <label>Description:</label>
                                            <input type="text" className="form-control" ref="desc2" onChange={this.handleDesc2}
                                                   placeholder="Description"/>
                                        </div>

                                        <div className="form-group">
                                            <span className="error">*</span><label>Company Name 1:</label>
                                            <input type="text" className="form-control" ref="cname1" onChange={this.handleCname1}
                                                   placeholder="Enter Company Name"/>
                                        </div>
                                        <div className="form-group">
                                            <span className="error">*</span><label>Post:</label>
                                            <input type="text" className="form-control" ref="post1" onChange={this.handlepost1}
                                                   placeholder="Enter Post"/>
                                        </div>
                                        <div className="form-group">
                                            <span className="error">*</span>   <label>Description:</label>
                                            <input type="text" className="form-control" ref="descr1" onChange={this.handleDescr1}
                                                   placeholder="Description"/>
                                        </div>
                                        <div className="form-group form-inline">
                                            <span className="error">*</span> <label>From:</label>
                                            <select className="form-control" id="sel1" ref="cname1_from" onChange={this.handlecnameFrom}>
                                                <option>1960</option>
                                                <option>1961</option>
                                                <option>1962</option>
                                                <option>1963</option>
                                                <option>1964</option>
                                                <option>1965</option>
                                                <option>1966</option>
                                                <option>1967</option>
                                                <option>1968</option>
                                                <option>1969</option>
                                                <option>1970</option>
                                                <option>1971</option>
                                                <option>1972</option>
                                                <option>1973</option>
                                                <option>1974</option>
                                                <option>1975</option>
                                                <option>1976</option>
                                                <option>1977</option>
                                                <option>1978</option>
                                                <option>1979</option>
                                                <option>1980</option>
                                                <option>1981</option>
                                                <option>1982</option>
                                                <option>1983</option>
                                                <option>1984</option>
                                                <option>1985</option>
                                                <option>1986</option>
                                                <option>1987</option>
                                                <option>1988</option>
                                                <option>1989</option>
                                                <option>1990</option>
                                                <option>1991</option>
                                                <option>1992</option>
                                                <option>1993</option>
                                                <option>1994</option>
                                                <option>1995</option>
                                                <option>1996</option>
                                                <option>1997</option>
                                                <option>1998</option>
                                                <option>1999</option>
                                                <option>2001</option>
                                                <option>2002</option>
                                                <option>2003</option>
                                                <option>2004</option>
                                                <option>2005</option>
                                                <option>2006</option>
                                                <option>2007</option>
                                                <option>2008</option>
                                                <option>2009</option>
                                                <option>2010</option>
                                                <option>2011</option>
                                                <option>2012</option>
                                                <option>2013</option>
                                                <option>2014</option>
                                                <option>2015</option>
                                                <option>2016</option>
                                                <option>2017</option>
                                            </select>
                                            <span className="error">*</span> <label> To:</label>
                                            <select className="form-control" id="sel1" ref="cname1_to" onChange={this.handlecnameTo}>
                                                <option>1960</option>
                                                <option>1961</option>
                                                <option>1962</option>
                                                <option>1963</option>
                                                <option>1964</option>
                                                <option>1965</option>
                                                <option>1966</option>
                                                <option>1967</option>
                                                <option>1968</option>
                                                <option>1969</option>
                                                <option>1970</option>
                                                <option>1971</option>
                                                <option>1972</option>
                                                <option>1973</option>
                                                <option>1974</option>
                                                <option>1975</option>
                                                <option>1976</option>
                                                <option>1977</option>
                                                <option>1978</option>
                                                <option>1979</option>
                                                <option>1980</option>
                                                <option>1981</option>
                                                <option>1982</option>
                                                <option>1983</option>
                                                <option>1984</option>
                                                <option>1985</option>
                                                <option>1986</option>
                                                <option>1987</option>
                                                <option>1988</option>
                                                <option>1989</option>
                                                <option>1990</option>
                                                <option>1991</option>
                                                <option>1992</option>
                                                <option>1993</option>
                                                <option>1994</option>
                                                <option>1995</option>
                                                <option>1996</option>
                                                <option>1997</option>
                                                <option>1998</option>
                                                <option>1999</option>
                                                <option>2001</option>
                                                <option>2002</option>
                                                <option>2003</option>
                                                <option>2004</option>
                                                <option>2005</option>
                                                <option>2006</option>
                                                <option>2007</option>
                                                <option>2008</option>
                                                <option>2009</option>
                                                <option>2010</option>
                                                <option>2011</option>
                                                <option>2012</option>
                                                <option>2013</option>
                                                <option>2014</option>
                                                <option>2015</option>
                                                <option>2016</option>
                                                <option>2017</option>
                                            </select>
                                        </div>
                                        <div className="form-group">
                                            <label>Company Name 2:</label>
                                            <input type="text" className="form-control" ref="cname2"onChange={this.handleCname2}
                                                   placeholder="Company name"/>
                                        </div>
                                        <div className="form-group">
                                            <label>Post:</label>
                                            <input type="text" className="form-control" ref="post2"onChange={this.handlepost2}
                                                   placeholder="Enter Post"/>
                                        </div>
                                        <div className="form-group">
                                            <label>Description:</label>
                                            <input type="text" className="form-control" ref="descr2" onChange={this.handleDescr2}
                                                   placeholder="Description"/>
                                        </div>
                                        <div className="form-group form-inline">
                                            <label>From:</label>
                                            <select className="form-control" id="sel1" ref="cname2_from">
                                                <option>1960</option>
                                                <option>1961</option>
                                                <option>1962</option>
                                                <option>1963</option>
                                                <option>1964</option>
                                                <option>1965</option>
                                                <option>1966</option>
                                                <option>1967</option>
                                                <option>1968</option>
                                                <option>1969</option>
                                                <option>1970</option>
                                                <option>1971</option>
                                                <option>1972</option>
                                                <option>1973</option>
                                                <option>1974</option>
                                                <option>1975</option>
                                                <option>1976</option>
                                                <option>1977</option>
                                                <option>1978</option>
                                                <option>1979</option>
                                                <option>1980</option>
                                                <option>1981</option>
                                                <option>1982</option>
                                                <option>1983</option>
                                                <option>1984</option>
                                                <option>1985</option>
                                                <option>1986</option>
                                                <option>1987</option>
                                                <option>1988</option>
                                                <option>1989</option>
                                                <option>1990</option>
                                                <option>1991</option>
                                                <option>1992</option>
                                                <option>1993</option>
                                                <option>1994</option>
                                                <option>1995</option>
                                                <option>1996</option>
                                                <option>1997</option>
                                                <option>1998</option>
                                                <option>1999</option>
                                                <option>2001</option>
                                                <option>2002</option>
                                                <option>2003</option>
                                                <option>2004</option>
                                                <option>2005</option>
                                                <option>2006</option>
                                                <option>2007</option>
                                                <option>2008</option>
                                                <option>2009</option>
                                                <option>2010</option>
                                                <option>2011</option>
                                                <option>2012</option>
                                                <option>2013</option>
                                                <option>2014</option>
                                                <option>2015</option>
                                                <option>2016</option>
                                                <option>2017</option>
                                            </select>
                                            <label> To:</label>
                                            <select className="form-control" id="sel1" ref="cname2_to">
                                                <option>1960</option>
                                                <option>1961</option>
                                                <option>1962</option>
                                                <option>1963</option>
                                                <option>1964</option>
                                                <option>1965</option>
                                                <option>1966</option>
                                                <option>1967</option>
                                                <option>1968</option>
                                                <option>1969</option>
                                                <option>1970</option>
                                                <option>1971</option>
                                                <option>1972</option>
                                                <option>1973</option>
                                                <option>1974</option>
                                                <option>1975</option>
                                                <option>1976</option>
                                                <option>1977</option>
                                                <option>1978</option>
                                                <option>1979</option>
                                                <option>1980</option>
                                                <option>1981</option>
                                                <option>1982</option>
                                                <option>1983</option>
                                                <option>1984</option>
                                                <option>1985</option>
                                                <option>1986</option>
                                                <option>1987</option>
                                                <option>1988</option>
                                                <option>1989</option>
                                                <option>1990</option>
                                                <option>1991</option>
                                                <option>1992</option>
                                                <option>1993</option>
                                                <option>1994</option>
                                                <option>1995</option>
                                                <option>1996</option>
                                                <option>1997</option>
                                                <option>1998</option>
                                                <option>1999</option>
                                                <option>2001</option>
                                                <option>2002</option>
                                                <option>2003</option>
                                                <option>2004</option>
                                                <option>2005</option>
                                                <option>2006</option>
                                                <option>2007</option>
                                                <option>2008</option>
                                                <option>2009</option>
                                                <option>2010</option>
                                                <option>2011</option>
                                                <option>2012</option>
                                                <option>2013</option>
                                                <option>2014</option>
                                                <option>2015</option>
                                                <option>2016</option>
                                                <option>2017</option>
                                            </select>
                                        </div>

                                        <div className="form-group">

                                            <span className="error">*</span><label>College Name:</label>
                                            <input type="comment" className="form-control" ref="college_name" onChange={this.handleCollege_name}
                                                   placeholder="College Name"/>
                                        </div>
                                        <div className="form-group">
                                            <span className="error">*</span> <label>Major:</label>
                                            <input type="comment" className="form-control" ref="college_major" onChange={this.handlecmajor}
                                                   placeholder="Major Subject"/>
                                        </div>
                                        <div className="form-group form-inline">
                                            <span className="error">*</span> <label>From:</label>
                                            <select className="form-control" id="sel1" ref="from_college" onChange={this.handlecFrom}>
                                                <option>1960</option>
                                                <option>1961</option>
                                                <option>1962</option>
                                                <option>1963</option>
                                                <option>1964</option>
                                                <option>1965</option>
                                                <option>1966</option>
                                                <option>1967</option>
                                                <option>1968</option>
                                                <option>1969</option>
                                                <option>1970</option>
                                                <option>1971</option>
                                                <option>1972</option>
                                                <option>1973</option>
                                                <option>1974</option>
                                                <option>1975</option>
                                                <option>1976</option>
                                                <option>1977</option>
                                                <option>1978</option>
                                                <option>1979</option>
                                                <option>1980</option>
                                                <option>1981</option>
                                                <option>1982</option>
                                                <option>1983</option>
                                                <option>1984</option>
                                                <option>1985</option>
                                                <option>1986</option>
                                                <option>1987</option>
                                                <option>1988</option>
                                                <option>1989</option>
                                                <option>1990</option>
                                                <option>1991</option>
                                                <option>1992</option>
                                                <option>1993</option>
                                                <option>1994</option>
                                                <option>1995</option>
                                                <option>1996</option>
                                                <option>1997</option>
                                                <option>1998</option>
                                                <option>1999</option>
                                                <option>2001</option>
                                                <option>2002</option>
                                                <option>2003</option>
                                                <option>2004</option>
                                                <option>2005</option>
                                                <option>2006</option>
                                                <option>2007</option>
                                                <option>2008</option>
                                                <option>2009</option>
                                                <option>2010</option>
                                                <option>2011</option>
                                                <option>2012</option>
                                                <option>2013</option>
                                                <option>2014</option>
                                                <option>2015</option>
                                                <option>2016</option>
                                                <option>2017</option>
                                            </select>
                                            <span className="error">*</span><label> To:</label>
                                            <select className="form-control" id="sel1" ref="to_college" onChange={this.handlecTo}>
                                                <option>1960</option>
                                                <option>1961</option>
                                                <option>1962</option>
                                                <option>1963</option>
                                                <option>1964</option>
                                                <option>1965</option>
                                                <option>1966</option>
                                                <option>1967</option>
                                                <option>1968</option>
                                                <option>1969</option>
                                                <option>1970</option>
                                                <option>1971</option>
                                                <option>1972</option>
                                                <option>1973</option>
                                                <option>1974</option>
                                                <option>1975</option>
                                                <option>1976</option>
                                                <option>1977</option>
                                                <option>1978</option>
                                                <option>1979</option>
                                                <option>1980</option>
                                                <option>1981</option>
                                                <option>1982</option>
                                                <option>1983</option>
                                                <option>1984</option>
                                                <option>1985</option>
                                                <option>1986</option>
                                                <option>1987</option>
                                                <option>1988</option>
                                                <option>1989</option>
                                                <option>1990</option>
                                                <option>1991</option>
                                                <option>1992</option>
                                                <option>1993</option>
                                                <option>1994</option>
                                                <option>1995</option>
                                                <option>1996</option>
                                                <option>1997</option>
                                                <option>1998</option>
                                                <option>1999</option>
                                                <option>2001</option>
                                                <option>2002</option>
                                                <option>2003</option>
                                                <option>2004</option>
                                                <option>2005</option>
                                                <option>2006</option>
                                                <option>2007</option>
                                                <option>2008</option>
                                                <option>2009</option>
                                                <option>2010</option>
                                                <option>2011</option>
                                                <option>2012</option>
                                                <option>2013</option>
                                                <option>2014</option>
                                                <option>2015</option>
                                                <option>2016</option>
                                                <option>2017</option>
                                            </select>
                                        </div>

                                        <div className="form-group">
                                            <span className="error">*</span>  <label>University Name:</label>
                                            <input type="comment" className="form-control" ref="university_name" onChange={this.handleUni_name}
                                                   placeholder="University Name"/>
                                        </div>
                                        <div className="form-group">
                                            <span className="error">*</span>   <label>Major:</label>
                                            <input type="comment" className="form-control" ref="university_major" onChange={this.handleumajor}
                                                   placeholder="Major subject"/>
                                        </div>
                                        <div className="form-group form-inline">
                                            <span className="error">*</span> <label>From:</label>
                                            <select className="form-control" id="sel1" ref="from_university" onChange={this.handleuFrom}>
                                                <option>1960</option>
                                                <option>1961</option>
                                                <option>1962</option>
                                                <option>1963</option>
                                                <option>1964</option>
                                                <option>1965</option>
                                                <option>1966</option>
                                                <option>1967</option>
                                                <option>1968</option>
                                                <option>1969</option>
                                                <option>1970</option>
                                                <option>1971</option>
                                                <option>1972</option>
                                                <option>1973</option>
                                                <option>1974</option>
                                                <option>1975</option>
                                                <option>1976</option>
                                                <option>1977</option>
                                                <option>1978</option>
                                                <option>1979</option>
                                                <option>1980</option>
                                                <option>1981</option>
                                                <option>1982</option>
                                                <option>1983</option>
                                                <option>1984</option>
                                                <option>1985</option>
                                                <option>1986</option>
                                                <option>1987</option>
                                                <option>1988</option>
                                                <option>1989</option>
                                                <option>1990</option>
                                                <option>1991</option>
                                                <option>1992</option>
                                                <option>1993</option>
                                                <option>1994</option>
                                                <option>1995</option>
                                                <option>1996</option>
                                                <option>1997</option>
                                                <option>1998</option>
                                                <option>1999</option>
                                                <option>2001</option>
                                                <option>2002</option>
                                                <option>2003</option>
                                                <option>2004</option>
                                                <option>2005</option>
                                                <option>2006</option>
                                                <option>2007</option>
                                                <option>2008</option>
                                                <option>2009</option>
                                                <option>2010</option>
                                                <option>2011</option>
                                                <option>2012</option>
                                                <option>2013</option>
                                                <option>2014</option>
                                                <option>2015</option>
                                                <option>2016</option>
                                                <option>2017</option>
                                            </select>
                                            <span className="error">*</span>  <label> To:</label>
                                            <select className="form-control" id="sel1" ref="to_university" onChange={this.handleuTo}>
                                                <option>1960</option>
                                                <option>1961</option>
                                                <option>1962</option>
                                                <option>1963</option>
                                                <option>1964</option>
                                                <option>1965</option>
                                                <option>1966</option>
                                                <option>1967</option>
                                                <option>1968</option>
                                                <option>1969</option>
                                                <option>1970</option>
                                                <option>1971</option>
                                                <option>1972</option>
                                                <option>1973</option>
                                                <option>1974</option>
                                                <option>1975</option>
                                                <option>1976</option>
                                                <option>1977</option>
                                                <option>1978</option>
                                                <option>1979</option>
                                                <option>1980</option>
                                                <option>1981</option>
                                                <option>1982</option>
                                                <option>1983</option>
                                                <option>1984</option>
                                                <option>1985</option>
                                                <option>1986</option>
                                                <option>1987</option>
                                                <option>1988</option>
                                                <option>1989</option>
                                                <option>1990</option>
                                                <option>1991</option>
                                                <option>1992</option>
                                                <option>1993</option>
                                                <option>1994</option>
                                                <option>1995</option>
                                                <option>1996</option>
                                                <option>1997</option>
                                                <option>1998</option>
                                                <option>1999</option>
                                                <option>2001</option>
                                                <option>2002</option>
                                                <option>2003</option>
                                                <option>2004</option>
                                                <option>2005</option>
                                                <option>2006</option>
                                                <option>2007</option>
                                                <option>2008</option>
                                                <option>2009</option>
                                                <option>2010</option>
                                                <option>2011</option>
                                                <option>2012</option>
                                                <option>2013</option>
                                                <option>2014</option>
                                                <option>2015</option>
                                                <option>2016</option>
                                                <option>2017</option>
                                            </select>
                                        </div>
                                        <div className="form-group">
                                            <span className="error">*</span> <label>Languages:</label>
                                            <input type="text" className="form-control" ref="languages" onChange={this.handleChangelanguage}
                                                   placeholder="Languages"/>
                                        </div>
                                        <div className="form-group">
                                            <label>Image:</label>
                                            <form onSubmit={this._handleSubmit}>
                                                <input type="file" onChange={this._handleImageChange} ref="image"/>
                                                {/*<button type="submit" onClick={this._handleSubmit}>Upload Image</button>*/}
                                            </form>
                                        </div>
                                        <button type="submit" className="btn btn-default" data-dismiss="modal"
                                                disabled={!this.state.name|| !this.state.Dev||!this.state.Dob||!this.state.contact||!this.state.email||!this.state.address||!this.state.motiv||
                                                !this.state.technicalskills|| !this.state.technicalskills2||!this.state.Cname1||!this.state.post1||!this.state.Descr1||!this.state.Cname2||
                                                !this.state.post2||!this.state.Descr2||
                                                !this.state.language||!this.state.College_name||!this.state.cmajor||!this.state.cFrom||!this.state.cTo||!this.state.Uni_name||!this.state.umajor||
                                                !this.state.uFrom||!this.state.uTo||!this.state.Desc1||!this.state.Desc2}
                                                onClick={this.handleSubmit.bind(this)}>Submit
                                        </button>
                                    </form>
                                </div>
                                <div className="modal-footer">
                                    <button type="button" className="btn btn-default" data-dismiss="modal">Close
                                    </button>
                                </div>
                            </div>

                        </div>
                    </div>
                    <div className="modal fade modal3" id="myModal2" role="dialog">
                        <div className="modal-dialog modal-xl">

                            <div className="modal-content">
                                <div className="modal-header">
                                    <button type="button" className="close" data-dismiss="modal">&times;</button>
                                </div>
                                <div className="modal-body">
                                    <Layout31 huzaifa={huzaifa} />
                                </div>
                                <div className="modal-footer">
                                    <button type="button" className="btn btn-default" data-dismiss="modal">Close
                                    </button>
                                </div>
                            </div>

                        </div>
                    </div>


                    <div>

                    </div>
                </div>
            </div>

        )
    }
}
Layout3_a1.propTypes = {
    tasks: PropTypes.array.isRequired,
    huzaifa:PropTypes.string.isRequired,

};

export default createContainer(() => {
    return {
        tasks: Tasks19.find({}).fetch(),
    };
}, Layout3_a1);