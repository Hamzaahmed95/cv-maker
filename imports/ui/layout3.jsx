
import React, { Component, PropTypes } from 'react';
import { createContainer } from 'meteor/react-meteor-data';
import './../../client/Stylesheet/CV3.css'

class Layout3 extends Component{
    render() {
        const style1 = {width:'150%'};
        const style2 = {display:'inline-block',width:'3%'};
        const style3 = {display:'inline-block',width:'10%'};
        const style4 = {display:'inline-block',width:'3%'};
        const style5 = {display:'inline-block',width:'20%'};
        const style6 = {display:'inline-block',width:'3%'};
        const style7 = {display:'inline-block',width:'15%'};
        const style8 = {display:'inline-block',width:'3%'};
        const style9 = {display:'inline-block',width:'10%'};
        return (
            <div>
                <div className="containCV3">

                    <div className="wrapperCV3">

                        <div className="wrap2CV3">

                            <div className="row MainrowCV3">

                                <div className="row1CV3">

                                    <div className="col-sm-8 paraCV3">

                                        <p className="whiteFOntCV3 headingCV3 p11">{this.props.task.name}</p> <p className="whiteFOntCV3 headingCV3 p1CV3">{this.props.task.Dev}</p>


                                        <p className="whiteFOntCV3 headingCV3">
                                            {this.props.task.motiv}</p></div>
                                    <div className="col-sm-4">
                                        <img className="img-circle image1CV3" src={this.props.huzaifa}/></div>
                                </div>
                            </div>

                        </div>

                        <div className="row">




                            <div style={style1} className="wrapCV3">

                                <div style={style2} className="whiteFOntCV3"><img src="/images/contact.png"/></div>
                                <div style={style3} className="whiteFOntCV3"><p>{this.props.task.contact}</p></div>

                                <div style={style4} className="whiteFOntCV3"><img src="/images/email.png"/></div>
                                <div style={style5} className="whiteFOntCV3"><p>{this.props.task.email}</p></div>

                                <div style={style6}><img src="/images/location.png"/></div>
                                <div style={style7} className="whiteFOntCV3"><p>{this.props.task.address}</p></div>

                                <div style={style8}><img src="/images/email.png"/></div>
                                <div style={style9} className="whiteFOntCV3"><p>{this.props.task.Dob}</p></div>
                            </div>

                        </div>
                    </div>
                    <div className="wrapper2CV3">
                        <div className="row row0CV3">
                            <div className="col-md-6 pull-left blueCV3"><h3>Work Experience</h3></div>
                            <div className="col-md-4 pull-right blueCV3"><h3>Technical Skills</h3></div></div>

                        <div className="row MainrowCV3">
                            <div className="col-md-6 pull-left blueCV3">
                                <div className="container1CV3">



                                    <div className="timeline">

                                        <div className="line text-muted"/>
                                        <article className="panel panel-info panel-outline">

                                            <div className="panel-heading icon">
                                                <i className="glyphicon glyphicon-info-sign"/>
                                            </div>
                                            <div className="panel-body blackCV3">
                                                <strong>{this.props.task.cname1}</strong>{this.props.task.cname1_from}-{this.props.task.cname1_to}
                                                <br/>
                                                    <cite>{this.props.task.post1}</cite>
                                                    <p>{this.props.task.descr1}</p>
                                            </div>

                                        </article>
                                        <article className="panel panel-info panel-outline">

                                            <div className="panel-heading icon">
                                                <i className="glyphicon glyphicon-info-sign"/>
                                            </div>
                                            <div className="panel-body blackCV3">
                                                <strong>{this.props.task.cname2}</strong> {this.props.task.cname2_from}-{this.props.task.cname2_to}
                                                <br/>
                                                    <cite>{this.props.task.post2}</cite>
                                                    <p>{this.props.task.descr2}</p>
                                            </div>

                                        </article>

                                    </div>

                                </div>
                            </div>
                            <div className="col-md-6 pull-right blueCV3">

                                <div className="container1CV3">
                                    <ul className="timeline blackCV3">


                                        <li>
                                            <div className="timeline-badge danger"><i className="glyphicon glyphicon-credit-card"/></div>
                                            <div className="timeline-panel">
                                                <div className="timeline-heading">
                                                    <h4 className="timeline-title">{this.props.task.technicalskills}</h4>
                                                </div>
                                                <div className="timeline-body">
                                                    <p>{this.props.task.desc1}</p> </div>
                                            </div>
                                        </li>
                                        <li>
                                            <div className="timeline-badge danger"><i className="glyphicon glyphicon-credit-card"/></div>
                                            <div className="timeline-panel">
                                                <div className="timeline-heading">
                                                    <h4 className="timeline-title">{this.props.task.technicalskills2}</h4>
                                                </div>
                                                <div className="timeline-body">
                                                    <p>{this.props.task.desc2} </p>   </div>
                                            </div>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div className="row row0CV3">
                            <div className="col-md-6 pull-left blueCV3"><h3>Education</h3></div>
                            <div className="col-md-3 pull-right blueCV3"><h3>Languages</h3></div></div>
                        <div className="row MainrowCV3">
                            <div className="col-md-6 pull-left blueCV3">
                                <div className="container1">


                                    <div className="timeline">


                                        <div className="line text-muted"/>
                                        <article className="panel panel-info panel-outline">

                                            <div className="panel-heading icon">
                                                <i className="glyphicon glyphicon-info-sign"/>
                                            </div>
                                            <div className="panel-body blackCV3">
                                                <strong>{this.props.task.university_name}</strong> {this.props.task.uFrom}-{this.props.task.uTo}
                                                <br/>
                                                    <cite>{this.props.task.university_major}</cite>
                                            </div>

                                        </article>
                                        <article className="panel panel-info panel-outline">

                                            <div className="panel-heading icon">
                                                <i className="glyphicon glyphicon-info-sign"/>
                                            </div>
                                            <div className="panel-body blackCV3">
                                                <strong>{this.props.task.college_name}</strong>{this.props.task.cFrom}-{this.props.task.cTo}
                                                <br/>
                                                    <cite>{this.props.task.college_major}</cite>

                                            </div>

                                        </article>

                                    </div>

                                </div>
                            </div>
                            <div className="col-md-6 pull-right blueCV3">

                                <div className="container1">
                                    <ul className="timeline blackCV3">


                                        <li>
                                            <div className="timeline-badge danger"><i className="glyphicon glyphicon-credit-card"/></div>
                                            <div className="timeline-panel">
                                                <div className="timeline-heading">
                                                    <h4 className="timeline-title">{this.props.task.languages}</h4>
                                                </div>
                                                <div className="timeline-body">
                                                </div></div>
                                        </li>

                                    </ul>
                                </div>

                            </div>
                        </div>
                        <footer>
                            &copy; 2017 {this.props.task.name}
                        </footer>
                    </div>

                </div>
            </div>

        );
    }}

export default Layout3 ;