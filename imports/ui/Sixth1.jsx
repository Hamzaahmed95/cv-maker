import {Link} from 'react-router'
import ReactDOM from 'react-dom';
import React, { Component, PropTypes } from 'react';

import { Tasks12 } from '../api/tasks12.js';
import C7V from './C7V'
import Cv2 from './Cv2'
import { createContainer } from 'meteor/react-meteor-data';
class Sixth1 extends Component{

    renderTasks(){
        const huzaifa2=this.props.huzaifa;
        return this.props.tasks.map((task) => (

            <C7V key={task._id} task={task} huzaifa={huzaifa2}/>
        ));

    }


    render() {
        return (
            <div className="sixth container">
                {this.renderTasks()}
            </div>
        );
    }}


Sixth1.propTypes = {
    huzaifa:PropTypes.string.isRequired,
};

export default createContainer(() => {
    return {
        tasks: Tasks12.find({}, { sort: { createdAt: -1 } }).fetch(),


    };
}, Sixth1);