import {Link} from 'react-router'
import ReactDOM from 'react-dom';
import React, { Component, PropTypes } from 'react';

import { Tasks6 } from '../api/tasks6.js';
import Cv7 from './Cv7'
import Cv2 from './Cv2'
import { createContainer } from 'meteor/react-meteor-data';
class Sixth extends Component{

    renderTasks(){
        const huzaifa2=this.props.huzaifa;
        return this.props.tasks.map((task) => (

            <Cv7 key={task._id} task={task} huzaifa={huzaifa2}/>

        ));
    }


    render() {
        return (
            <div className=" sixth container">
                {this.renderTasks()}
            </div>
        );
    }}


Sixth.propTypes = {
    huzaifa:PropTypes.string.isRequired,
};

export default createContainer(() => {
    return {
        tasks: Tasks6.find({}).fetch(),

    };
}, Sixth);