import {Link} from 'react-router'
import App from './App'
import { Tasks } from '../api/tasks.js';
import React, { Component, PropTypes } from 'react';
import { createContainer } from 'meteor/react-meteor-data';

import '/client/Stylesheet/Cv.css';
class Cv extends Component{
    render() {
        return (
            <div>
            <div className="wrapper">

                <aside>
                    <div className="asideBlue">
                    <section className="popular-recipes">
                        {console.log("here")}
                        <img className="img-circle" src={this.props.huzaifa}/>
                            <h2>Profile Summary</h2>
                            <a href="">Motivation</a>
                            <a href="">Technical Skilss</a>
                            <a href="">Strength</a>
                            <a href="">Extra-Curricular</a>
                            <a href="">Electives</a>
                            <a href="">Experience</a>
                            <a href="">Achievements</a>
                    </section>
                    <section className="contact-details">
                        <h2>Contact</h2>
                        <p>Email : {this.props.task.email}<br /></p>
                            <p>Address :<br />
                                {this.props.task.address},Karachi<br />
                                Pakistan</p>
                    </section></div>
                </aside>
                <div className="helloBlue">
                    <h2 className="heading3"><cite>{this.props.task.name}</cite></h2>

                        <br/><h4 className="heading2">Contact detail:{this.props.task.contact}  /  {this.props.task.email}</h4>
                </div>
                <section className="courses">
                    <article>
                        <div className="para1 border">
                            <h3 className="contact">Motivation</h3>
                            <p>{this.props.task.motiv}</p></div>
                    </article>
                    <article>
                        <div className="para1 border">
                            <h3 className="contact">Technical Skills</h3>
                            <p>{this.props.task.technicalskills}</p></div>
                    </article><article>
                    <div className="para1 border">
                        <h3 className="contact">Strength</h3>
                        <ul>
                            <li>{this.props.task.Strengths}</li>

                        </ul></div>
                </article>
                    <article>
                        <div className="para1 border">
                            <h3 className="contact">Electives:</h3>
                            <ul>
                                <li>{this.props.task.elective}
                                </li>

                            </ul></div>
                    </article>
                    <article>
                        <div className="para1 border">
                            <h3 className="contact">Hobby</h3>
                            <ul>
                                <li>{this.props.task.hobby}
                                </li>

                            </ul></div>
                    </article>
                    <article>
                        <div className="para1 border">
                            <h3 className="contact">Achievements:</h3>
                            <ul>
                                <li>{this.props.task.achievments}
                                </li>

                            </ul></div>
                    </article>

                </section>

                <footer>
                    &copy; 2017 {this.props.task.name}
                </footer>
            </div></div>
    );
    }}

    export default  Cv ;