import React, {Component,PropTypes} from 'react';
import { createContainer } from 'meteor/react-meteor-data';
import {Link} from 'react-router'

class Layout2V2 extends Component{
    render()
    {
        return (
            <div className="hamza container-fluid">
                <div className="text-center">
                    <img src="images/cv.png" className="rounded" alt="..." height="10%" width="10%"/>
                </div>
                <div className="text-center">
                    <h4 className="text-center c2">CV MAKER</h4>
                </div>
                <div className="hamza22">
                    <div className="text-center">
                        <p className="text-center c3" height="20%" width="20%">What do you want to make today?</p>
                    </div>
                    <div className="row hamza2row">
                        <div className="col-md-4">
                            <Link to ='/layout1'>
                                <div className="img-hover lMaroon img-rounded">
                                    <img src="images/l1.png" className="img-responsive img-fluid img-thumbnail center-block img-zoom scroll1" height="50%" width="50%"/>
                                    <div className="text-center">
                                        <h2 className="text-center3 para">Maroon</h2>
                                    </div>
                                </div>
                            </Link>
                        </div>
                        <div className="col-md-4">
                            <Link to ='/layout3'>
                                <div className="img-hover llightBlue img-rounded">
                                    <img src="images/l2.png" className="img-responsive img-fluid img-thumbnail center-block img-zoom scroll1" height="50%" width="50%"/>
                                    <div className="text-center">
                                        <h2 className="text-center3 para">Blue</h2>
                                    </div>
                                </div>
                            </Link>
                        </div>
                        <div className="col-md-4">
                            <Link to ='/layout2'> <div className="img-hover lPurple img-rounded"><img src="images/l3.png" className="img-responsive img-fluid img-thumbnail center-block img-zoom scroll1" height="50%" width="50%"/>  <div className="text-center">
                                <h2 className="text-center3 para">Purple</h2>
                            </div></div></Link>

                        </div>
                    </div>
                    <div className="row hamza2row">
                        <div className="col-md-4">
                            <Link to ='/layout4'>
                                <div className="img-hover lPink img-rounded">
                                    <img src="images/l4.png" className="img-responsive img-fluid img-thumbnail center-block img-zoom scroll1" height="50%" width="50%"/>
                                    <div className="text-center">
                                        <h2 className="text-center3 para">Pink</h2>
                                    </div>
                                </div>
                            </Link>
                        </div>
                        <div className="col-md-4">
                            <Link to ='/layout5'>
                                <div className="img-hover lBlue img-rounded">
                                    <img src="images/l5.png" className="img-responsive img-fluid img-thumbnail center-block img-zoom scroll1" height="50%" width="50%"/>
                                    <div className="text-center">
                                        <h2 className="text-center3 para">Dark Blue</h2>
                                    </div>
                                </div>
                            </Link>
                        </div>
                        <div className="col-md-4">
                            <Link to ='/layout6'>
                                <div className="img-hover lGreen img-rounded">
                                    <img src="images/l6.png" className="img-responsive img-fluid img-thumbnail center-block img-zoom scroll1" height="50%" width="50%"/>
                                    <div className="text-center">
                                        <h2 className="text-center3 para">Dark Green</h2>
                                    </div>
                                </div>
                            </Link>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}
Layout2V2.propTypes = {
    tasks: PropTypes.array.isRequired,

};

export default createContainer(() => {
    return {
    };
}, Layout2V2);