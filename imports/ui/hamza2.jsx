import React, {Component,PropTypes} from 'react';
import {Meteor} from 'meteor/meteor'
import { createContainer } from 'meteor/react-meteor-data'
import Task from './Task.jsx';
import { Tasks } from '../api/tasks.js';
import Cv from './Cv'
import Cv2 from './Cv2'
import Home1 from './Home1'
import {Link} from 'react-router'
import {Router}from 'react-router'
import ReactDOM from 'react-dom';
import ReactRouter from'./ReactRouter'
import hamza from './hamza'
import hamza3 from './hamza3'
class hamza2 extends Component{


    render() {
        return (
            <div className="hamza container">
                <div className="text-center">
                    <img src="images/cv.png" className="rounded" alt="..." height="10%" width="10%"/>
                </div>
                <div className="text-center">
                    <h4 className="text-center c2">CV MAKER</h4>
                </div>
                <div className="hamza22">
                    <div className="text-center">
                        <p className="text-center c3" height="20%" width="20%">What do you want to make today?</p>
                    </div>
                    <div className="row hamza2row">
                        <div className="col-md-4">
                            <Link to ='/hamza'>
                                <div className="img-hover asideBlue1 img-rounded">
                                    <img src="images/CV1.png" className="img-responsive img-fluid img-thumbnail center-block img-zoom scroll1" height="50%" width="50%"/>
                                    <div className="text-center">
                                        <h2 className="text-center3">Blue</h2>
                                    </div>
                                </div>
                            </Link>
                        </div>
                        <div className="col-md-4">
                            <Link to ='/hamza3'>
                                <div className="img-hover blue1 img-rounded">
                                    <img src="images/01.png" className="img-responsive img-fluid img-thumbnail center-block img-zoom scroll1" height="50%" width="50%"/>
                                    <div className="text-center">
                                        <h2 className="text-center3">Blue</h2>
                                    </div>
                                </div>
                            </Link>
                        </div>
                        <div className="col-md-4">
                            <Link to ='/hamza4'>
                                <div className="img-hover asidePurple1 img-rounded">
                                    <img src="images/02.jpg" className="img-responsive img-fluid img-thumbnail center-block img-zoom scroll1" height="50%" width="50%"/>
                                    <div className="text-center">
                                        <h2 className="text-center3">Purple</h2>
                                    </div>
                                </div>
                            </Link>
                        </div>
                    </div>
                    <div className="row hamza2row">
                        <div className="col-md-4">
                            <Link to ='/hamza6'>
                                <div className="img-hover asideYellow1 img-rounded">
                                    <img src="images/4.jpg" className="img-responsive img-fluid img-thumbnail center-block img-zoom scroll1" height="50%" width="50%"/>
                                    <div className="text-center">
                                        <h2 className="text-center3">Yellow</h2>
                                    </div>
                                </div>
                            </Link>
                        </div>
                        <div className="col-md-4">
                            <Link to ='/hamza7'>
                                <div className="img-hover asideGray2 img-rounded">
                                    <img src="images/5.jpg" className="img-responsive img-fluid img-thumbnail center-block img-zoom scroll1" height="50%" width="50%"/>
                                    <div className="text-center">
                                        <h2 className="text-center3">Gray</h2>
                                    </div>
                                </div>
                            </Link>
                        </div>
                        <div className="col-md-4">
                            <Link to ='/hamza8'>
                                <div className="img-hover black1 img-rounded">
                                    <img src="images/8.jpg" className="img-responsive img-fluid img-thumbnail center-block img-zoom scroll1" height="50%" width="50%"/>
                                    <div className="text-center">
                                        <h2 className="text-center3">Black</h2>
                                    </div>
                                </div>
                            </Link>
                        </div>
                    </div>
                    <div className="row hamza2row">
                        <div className="col-md-4">
                            <Link to ='/hamza10'>
                                <div className="img-hover asideGreen1 img-rounded">
                                    <img src="images/10.png" className="img-responsive img-fluid img-thumbnail center-block img-zoom scroll1" height="50%" width="50%"/>
                                    <div className="text-center">
                                        <h2 className="text-center3">Green</h2>
                                    </div>
                                </div>
                            </Link>
                        </div>
                        <div className="col-md-4">
                            <Link to ='/hamza11'>
                                <div className="img-hover asideMaroon1 img-rounded">
                                    <img src="images/11.png" className="img-responsive img-fluid img-thumbnail center-block img-zoom scroll1" height="50%" width="50%"/>
                                    <div className="text-center">
                                        <h2 className="text-center3">Red</h2>
                                    </div>
                                </div>
                            </Link>
                        </div>
                        <div className="col-md-4">
                            <Link to ='/hamza12'>
                                <div className="img-hover asideBrown1 img-rounded">
                                    <img src="images/12.png" className="img-responsive img-fluid img-thumbnail center-block img-zoom scroll1" height="50%" width="50%"/>
                                    <div className="text-center">
                                        <h2 className="text-center3">Brown</h2>
                                    </div>
                                </div>
                            </Link>
                        </div>
                    </div>
                    <div className="row hamza2row">
                        <div className="col-md-4">
                            <Link to ="/hamza13">
                                <div className="img-hover asideOrange1 img-rounded">
                                    <img src="images/13.png" className="img-responsive img-fluid img-thumbnail center-block img-zoom scroll1" height="50%" width="50%"/>
                                    <div className="text-center">
                                        <h2 className="text-center3">Orange</h2>
                                    </div>
                                </div>
                            </Link>
                        </div>
                        <div className="col-md-4">
                            <Link to ="/hamza9">
                                <div className="img-hover asidePink1 img-rounded">
                                    <img src="images/9.png" className="img-responsive img-fluid img-thumbnail center-block img-zoom scroll1" height="50%" width="50%"/>
                                    <div className="text-center">
                                        <h2 className="text-center3">Pink</h2>
                                    </div>
                                </div>
                            </Link>
                        </div>
                        <div className="col-md-4">
                            <Link to ="/hamza5">
                                <div className="img-hover asideGray3 img-rounded">
                                    <img src="images/3.jpg" className="img-responsive img-fluid img-thumbnail center-block img-zoom scroll1" height="50%" width="50%"/>
                                    <div className="text-center">
                                        <h2 className="text-center3">Gray</h2>
                                    </div>
                                </div>
                            </Link>
                        </div>
                    </div>
                </div>
            </div>

        )
    }
}

export default createContainer(() => {
    return {
    };
}, hamza2);