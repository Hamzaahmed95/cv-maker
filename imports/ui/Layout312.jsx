import {Link} from 'react-router'
import ReactDOM from 'react-dom';
import React, { Component, PropTypes } from 'react';

import { Tasks20 } from '../api/tasks20.js';
import Layout32 from './Layout32'
import Cv2 from './Cv2'
import { createContainer } from 'meteor/react-meteor-data';
class Layout312 extends Component{

    renderTasks(){
        const huzaifa2=this.props.huzaifa;

        return this.props.tasks.map((task) => (

            <Layout32 key={task._id} task={task}  huzaifa={huzaifa2}/>
        ));

    }


    render() {
        return (
            <div className="sixth container">
                <h1>hello</h1>
                {console.log("in render")}
                {this.renderTasks()}
            </div>
        );
    }}


Layout312.propTypes = {
    huzaifa:PropTypes.string.isRequired,
};

export default createContainer(() => {
    return {
        tasks: Tasks20.find({}, { sort: { createdAt: -1 } }).fetch(),


    };
}, Layout312);