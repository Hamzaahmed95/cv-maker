import {Link} from 'react-router'
import ReactDOM from 'react-dom';
import React, { Component, PropTypes } from 'react';

import { Tasks10 } from '../api/tasks10.js';
import C5V from './C5V'
import Cv2 from './Cv2'
import { createContainer } from 'meteor/react-meteor-data';
class Fourth1 extends Component{

    renderTasks(){
        const huzaifa2=this.props.huzaifa;
        return this.props.tasks.map((task) => (
            <C5V key={task._id} task={task} huzaifa={huzaifa2}/>
        ));

    }


    render() {
        return (
            <div className="sixth container">
                {this.renderTasks()}
            </div>
        );
    }}


Fourth1.propTypes = {
    huzaifa:PropTypes.string.isRequired,
};

export default createContainer(() => {
    return {
        tasks: Tasks10.find({}, { sort: { createdAt: -1 } }).fetch(),


    };
}, Fourth1);