import {Link} from 'react-router'
import ReactDOM from 'react-dom';
import React, { Component, PropTypes } from 'react';

import { Tasks17 } from '../api/tasks17.js';
import Cv4 from './Cv4'
import C17 from './C17'
import { createContainer } from 'meteor/react-meteor-data';
class Sixth2 extends Component{

    renderTasks(){

        const huzaifa2=this.props.huzaifa;
        return this.props.tasks.map((task) => (

            <C17 key={task._id} task={task} huzaifa={huzaifa2}/>

        ));

    }


    render() {
        return (
            <div className="sixth container">
                {/*console.log("in render"+this.props.huzaifa)*/}
                {/*<img src={this.props.huzaifa}/>*/}
                {this.renderTasks()}

            </div>
        );
    }}


Sixth2.propTypes = {
    huzaifa:PropTypes.string.isRequired,
};

export default createContainer(() => {
    return {
        tasks: Tasks17.find({}).fetch(),

    };
}, Sixth2);