import React, {Component,PropTypes} from 'react';
import { createContainer } from 'meteor/react-meteor-data'
import { Tasks } from '../api/tasks.js';
import {Link} from 'react-router'
let count=0;

class home extends Component{
    render()
    {
        return (
            <div className="container-fluid">
                <div className="home">
                    <header>
                        <div className="text-center">
                            <img src="images/cv.png" className="rounded" alt="..." height="30%" width="30%"/>
                        </div>
                        <div className="text-center">
                            <h2 className="text-center cblack">CV MAKER</h2>
                            <Link to="/hamza1"><a className="btn btn-danger btn-xl page-scroll">Get Started</a></Link>
                        </div>
                    </header>
                </div>
                <h2 className="text-center h1"><em>"A right Resume can open many doors."</em></h2>
            </div>
        )
    }
}
home.propTypes = {
    tasks: PropTypes.array.isRequired,
};

export default createContainer(() => {
    return {
        tasks: Tasks.find({}).fetch(),

    };
}, home);