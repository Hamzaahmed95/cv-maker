import {Link} from 'react-router'
import ReactDOM from 'react-dom';
import React, { Component, PropTypes } from 'react';

import { Tasks19 } from '../api/tasks19.js';
import Layout3 from './layout3'
import Cv2 from './Cv2'
import { createContainer } from 'meteor/react-meteor-data';
class Layout31 extends Component{

    renderTasks(){
        const huzaifa2=this.props.huzaifa;

        return this.props.tasks.map((task) => (

            <Layout3 key={task._id} task={task}  huzaifa={huzaifa2}/>
        ));

    }


    render() {
        return (
            <div className="sixth container">
                <h1>hello</h1>
                {console.log("in render")}
                {this.renderTasks()}
            </div>
        );
    }}


Layout31.propTypes = {
    huzaifa:PropTypes.string.isRequired,
};

export default createContainer(() => {
    return {
        tasks: Tasks19.find({}, { sort: { createdAt: -1 } }).fetch(),


    };
}, Layout31);