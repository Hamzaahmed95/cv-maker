import React, {Component,PropTypes} from 'react';
import {Meteor} from 'meteor/meteor'
import { createContainer } from 'meteor/react-meteor-data'
import Task from './Task.jsx';
import { Tasks3 } from '../api/tasks3.js';
import Cv from './Cv'
import Cv2 from './Cv2'
import Home1 from './Home1'
import {Link} from 'react-router'
import ReactDOM from 'react-dom';
import Third1 from './Third1'
class hamza4 extends Component{


    setTimeout(){
        this.setTimeout(this, 5000)
    }



    handleSubmit(event){

        // const url=huzaifa;
        //console.log ("url = "+url);

        event.preventDefault;
        // Find the text field via the React ref
        const name = ReactDOM.findDOMNode(this.refs.contact_name).value.trim();
        const contact = ReactDOM.findDOMNode(this.refs.contact_detail).value.trim();
        const email = ReactDOM.findDOMNode(this.refs.email).value.trim();
        const address = ReactDOM.findDOMNode(this.refs.address).value.trim();
        const motiv = ReactDOM.findDOMNode(this.refs.motivation).value.trim();
        const technicalskills = ReactDOM.findDOMNode(this.refs.skills).value.trim();
        const Strengths = ReactDOM.findDOMNode(this.refs.strength).value.trim();
        const hobby = ReactDOM.findDOMNode(this.refs.hobbies).value.trim();
        const elective = ReactDOM.findDOMNode(this.refs.elective).value.trim();
        const achievments = ReactDOM.findDOMNode(this.refs.achievements).value.trim();
        const image = ReactDOM.findDOMNode(this.refs.image).value.trim();

        Tasks3.insert({
            name,
            contact,
            email,
            address,
            motiv,
            technicalskills,
            Strengths,
            hobby,
            elective,
            achievments,
            image,
            //url,

            createdAt: new Date(), // current time
        });
        // Clear form
        ReactDOM.findDOMNode(this.refs.contact_name).value="";
        ReactDOM.findDOMNode(this.refs.contact_detail).value="";
        ReactDOM.findDOMNode(this.refs.email).value="";
        ReactDOM.findDOMNode(this.refs.address).value="";
        ReactDOM.findDOMNode(this.refs.motivation).value="";
        ReactDOM.findDOMNode(this.refs.skills).value="";
        ReactDOM.findDOMNode(this.refs.strength).value="";
        ReactDOM.findDOMNode(this.refs.hobbies).value="";
        ReactDOM.findDOMNode(this.refs.elective).value="";
        ReactDOM.findDOMNode(this.refs.achievements).value="";
        ReactDOM.findDOMNode(this.refs.image).value="";
    }

    _handleImageChange(e) {
        e.preventDefault();

        let reader = new FileReader();
        let file = e.target.files[0];

        reader.onloadend = () => {
            this.setState({
                file: file,
                imagePreviewUrl: reader.result
            });
        }

        reader.readAsDataURL(file)
    }
    constructor(props) {
        super(props);
        this.state = {
            file: '',
            imagePreviewUrl: '',
            error:'',
            error2:'',
            error4:'',
            name:'',
            contact:'',
            email:'',
            address:'',
            motiv:'',
            technicalskills:'',
            Strengths:'',
            hobby:'',
            elective:'',
            achievments:'',
        };
        this._handleImageChange = this._handleImageChange.bind(this);
        this._handleSubmit = this._handleSubmit.bind(this);
        this.handleChangename = this.handleChangename.bind(this);
        this.handleChangecontact = this.handleChangecontact.bind(this);
        this.handleChangeemail = this.handleChangeemail.bind(this);
        this.handleChangeaddress = this.handleChangeaddress.bind(this);
        this.handleChangemotiv = this.handleChangemotiv.bind(this);
        this.handleChangetechnicalskills = this.handleChangetechnicalskills.bind(this);
        this.handleChangeStrengths = this.handleChangeStrengths.bind(this);
        this.handleChangehobby = this.handleChangehobby.bind(this);
        this.handleChangeelective = this.handleChangeelective.bind(this);
        this.handleChangeachievments = this.handleChangeachievments.bind(this);

    }

    _handleSubmit(e) {
        //  e.preventDefault();
        //this.state.imagePreviewUrl = image
        // TODO: do something with -> this.state.file

    }


    renderTasks3() {
        return this.props.tasks.map((task) => (
            <Cv key={task._id} task={task}/>
        ));
    }



    renderTasks32() {
        return this.props.tasks.map((task) => (
            <Cv2 key={task._id} task={task}/>
        ));
    }
    showIt() {
        document.getElementsByClassName("div1").style.visibility = "visible";
        setTimeout("showIt()", 1000); // after 1 sec
    }
    handleChangename(e){
        console.log(e.target.value.match("^[a-zA-Z ]*$"));
        if(e.target.value.match("^[a-zA-Z ]*$")!=null)
        {
            this.setState({name: e.target.value});
            this.setState({error:""});
        }
        else
        {this.setState({error:"Only alphabets allowed!"});

            this.setState({name:''});
            console.log("error"+this.state.error);}

    }
    handleChangecontact(e){
        console.log(e.target.value.match("^[0-9]*$"));
        if(e.target.value.match("^[0-9]*$")!=null){
            this.setState({contact: e.target.value});
            this.setState({error2:""});
        }
        else
        {this.setState({error2:"Only numbers allowed!"});

            this.setState({contact:''});
            console.log("error2"+this.state.error2);}

    }
    handleChangeemail(e){
        console.log("here->"+e.target.value.match(/^[a-zA-Z0-9]+@[a-zA-Z0-9]+\.[A-Za-z]+$/));
        if(/^[A-Z0-9'.1234z_%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test(e.target.value)){
            this.setState({email: e.target.value});
            this.setState({error4:""});
        }
        else
        {this.setState({error4:"Invalid Email!"});
            this.setState({email: ''});
            console.log("error4"+this.state.error4);}

    }
    handleChangeaddress(e){
        this.setState({address: e.target.value});
        console.log("address"+this.state.address);
    }
    handleChangemotiv (e){
        this.setState({motiv: e.target.value});
        console.log("motiv"+this.state.motiv);
    }
    handleChangetechnicalskills(e){
        this.setState({technicalskills: e.target.value});
        console.log("technicalskills"+this.state.technicalskills);
    }
    handleChangeStrengths(e){
        this.setState({Strengths: e.target.value});
        console.log("Strengths"+this.state.Strengths);
    }
    handleChangehobby(e){
        this.setState({hobby: e.target.value});
        console.log("hobby"+this.state.hobby);
    }
    handleChangeelective(e){
        this.setState({elective: e.target.value});
        console.log("elective"+this.state.elective);
    }
    handleChangeachievments(e){
        this.setState({achievments: e.target.value});
        console.log("achievments"+this.state.achievments);
    }








    render()
    {
        let {imagePreviewUrl} = this.state;
        let $imagePreview = null;
        if (imagePreviewUrl) {

            $imagePreview = (<img src={imagePreviewUrl} />);

        }
        const huzaifa = imagePreviewUrl;
        return (
            <div className="hamza container">
                <div className="text-center">
                    <img src="images/cv.png" className="rounded" alt="..." height="10%" width="10%"/>
                </div>
                <div className="text-center">
                    <h4 className="text-center c2">CV MAKER</h4>
                </div>
                <div className="hamza21">

                    <div className="text-center">
                        <img className="rounded" width="45%" height="45%" src ="images/02.jpg" />
                    </div>
                    <div className="center">

                        <div className="row hamza3row">
                            <div className="col-sm-6 text-center" >
                                <button type="button" className="btn btn-primary btn-lg " data-toggle="modal" data-target="#myModal">
                                    Information
                                </button></div>

                            <div className="col-sm-6 text-center">
                                <button type="button" className=" btn btn-primary btn-lg " data-toggle="modal" data-target="#myModal2">
                                    Show CV
                                </button></div></div>
                    </div>

                    <div className="modal fade" id="myModal" role="dialog">
                        <div className="modal-dialog">

                            <div className="modal-content">
                                <div className="modal-header">
                                    <button type="button" className="close" data-dismiss="modal">&times;</button>
                                    <h4 className="modal-title">Add your Information</h4>
                                </div>
                                <div className="modal-body modalC1">
                                    <form>
                                        <div className="form-group">
                                            <span className="error">*</span><label>Name: </label><span className="error">{this.state.error}</span>
                                            <input type="text" className="form-control" ref="contact_name" onChange={this.handleChangename}
                                                   placeholder="Enter Name " name="[required]"/>
                                        </div>
                                        <div className="form-group">
                                            <span className="error">*</span><label>Contact No:</label><span className="error">{this.state.error2}</span>
                                            <input type="text" className="form-control" ref="contact_detail" onChange={this.handleChangecontact}
                                                   placeholder="Enter contact No"/>
                                        </div>
                                        <div className="form-group">
                                            <span className="error">*</span><label>Enter Email:</label><span className="error">{this.state.error4}</span>
                                            <input type="text" className="form-control" ref="email" onChange={this.handleChangeemail}
                                                   placeholder="Enter email"/>
                                        </div>
                                        <div className="form-group">
                                            <span className="error">*</span> <label>Enter Address:</label>
                                            <input type="text" className="form-control" ref="address" onChange={this.handleChangeaddress}
                                                   placeholder="Enter Address"/>
                                        </div>
                                        <div className="form-group">
                                            <span className="error">*</span> <label>Enter Motivation:</label>
                                            <input type="text" className="form-control" ref="motivation" onChange={this.handleChangemotiv}
                                                   placeholder="Enter your motivation"/>
                                        </div>
                                        <div className="form-group">
                                            <span className="error">*</span> <label>Technical Skills:</label>
                                            <input type="text" className="form-control" ref="skills" onChange={this.handleChangetechnicalskills}
                                                   placeholder="Your Technical skills"/>
                                        </div>
                                        <div className="form-group">
                                            <span className="error">*</span> <label>Strengths:</label>
                                            <input type="text" className="form-control" ref="strength" onChange={this.handleChangeStrengths}
                                                   placeholder="Enter your strengths"/>
                                        </div>
                                        <div className="form-group">
                                            <span className="error">*</span> <label>Hobbies:</label>
                                            <input type="text" className="form-control" ref="hobbies" onChange={this.handleChangehobby}
                                                   placeholder="Hobbies"/>
                                        </div>
                                        <div className="form-group">
                                            <span className="error">*</span> <label>Electives:</label>
                                            <input type="text" className="form-control" ref="elective" onChange={this.handleChangeelective}
                                                   placeholder="About electives"/>
                                        </div>
                                        <div className="form-group">
                                            <span className="error">*</span> <label>Achievements:</label>
                                            <input type="text" className="form-control" ref="achievements" onChange={this.handleChangeachievments}
                                                   placeholder="About your Achievements"/>
                                        </div>
                                        <div className="form-group">
                                            <label>Image:</label>
                                            <form onSubmit={this._handleSubmit}>
                                                <input type="file" onChange={this._handleImageChange} ref="image"/>
                                                {/*<button type="submit" onClick={this._handleSubmit}>Upload Image</button>*/}
                                            </form>
                                        </div>
                                        <button type="submit" className="btn btn-default" data-dismiss="modal" disabled={!this.state.name||!this.state.contact||!this.state.email||!this.state.address||!this.state.motiv||!this.state.technicalskills||
                                        !this.state.Strengths ||!this.state.hobby||!this.state.elective||!this.state.achievments}
                                                onClick={this.handleSubmit.bind(this)}>Submit
                                        </button>
                                    </form>
                                </div>
                                <div className="modal-footer">
                                    <button type="button" className="btn btn-default" data-dismiss="modal">Close
                                    </button>
                                </div>
                            </div>

                        </div>
                    </div>
                    <div className="modal fade modal3" id="myModal2" role="dialog">
                        <div className="modal-dialog modal-xl">

                            <div className="modal-content">
                                <div className="modal-header">
                                    <button type="button" className="close" data-dismiss="modal">&times;</button>
                                </div>
                                <div className="modal-body">
                                    <Third1 huzaifa={huzaifa}/>
                                </div>
                                <div className="modal-footer">
                                    <button type="button" className="btn btn-default" data-dismiss="modal">Close
                                    </button>
                                </div>
                            </div>

                        </div>
                    </div>


                    <div>

                    </div>
                </div>
            </div>

        )
    }
}
hamza4.propTypes = {
    tasks: PropTypes.array.isRequired,
    huzaifa:PropTypes.string.isRequired,

};

export default createContainer(() => {
    return {
        tasks: Tasks3.find({}).fetch(),
    };
}, hamza4);