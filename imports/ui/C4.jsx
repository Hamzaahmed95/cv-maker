import React, {Component,PropTypes} from 'react';
import {Meteor} from 'meteor/meteor'
import { createContainer } from 'meteor/react-meteor-data'
import Task from './Task.jsx';
import { Tasks10 } from '../api/tasks10.js';
import Cv from './Cv'
import Cv2 from './Cv2'
import Home1 from './Home1'
import Fourth1 from './Fourth1'
import {Link} from 'react-router'
import ReactDOM from 'react-dom';
class C4 extends Component{

    setTimeout(){
        this.setTimeout(this, 5000)
    }




    handleSubmit(event){
        event.preventDefault;




        // Find the text field via the React ref
        const name = ReactDOM.findDOMNode(this.refs.name).value.trim();
        const contact = ReactDOM.findDOMNode(this.refs.contact_number).value.trim();
        const tel_number = ReactDOM.findDOMNode(this.refs.tel_number).value.trim();
        const email = ReactDOM.findDOMNode(this.refs.email).value.trim();
        const address = ReactDOM.findDOMNode(this.refs.address).value.trim();
        const objective = ReactDOM.findDOMNode(this.refs.objective).value.trim();
        const birth_date = ReactDOM.findDOMNode(this.refs.birth_date).value.trim();
        const school_name = ReactDOM.findDOMNode(this.refs.school_name).value.trim();
        const school_major = ReactDOM.findDOMNode(this.refs.school_major).value.trim();
        const from_school = ReactDOM.findDOMNode(this.refs.from_school).value.trim();
        const to_school = ReactDOM.findDOMNode(this.refs.to_school).value.trim();
        const college_name = ReactDOM.findDOMNode(this.refs.college_name).value.trim();
        const college_major = ReactDOM.findDOMNode(this.refs.college_major).value.trim();
        const from_college = ReactDOM.findDOMNode(this.refs.from_college).value.trim();
        const to_college = ReactDOM.findDOMNode(this.refs.to_college).value.trim();
        const university_name = ReactDOM.findDOMNode(this.refs.university_name).value.trim();
        const university_major = ReactDOM.findDOMNode(this.refs.university_major).value.trim();
        const from_university = ReactDOM.findDOMNode(this.refs.from_university).value.trim();
        const to_university = ReactDOM.findDOMNode(this.refs.to_university).value.trim();

        const cname1 = ReactDOM.findDOMNode(this.refs.cname1).value.trim();
        const post1 = ReactDOM.findDOMNode(this.refs.post1).value.trim();
        const desc1 = ReactDOM.findDOMNode(this.refs.desc1).value.trim();
        const cname2 = ReactDOM.findDOMNode(this.refs.cname2).value.trim();
        const post2 = ReactDOM.findDOMNode(this.refs.post2).value.trim();
        const desc2 = ReactDOM.findDOMNode(this.refs.desc2).value.trim();
        const cname3 = ReactDOM.findDOMNode(this.refs.cname3).value.trim();
        const post3 = ReactDOM.findDOMNode(this.refs.post3).value.trim();
        const desc3 = ReactDOM.findDOMNode(this.refs.desc3).value.trim();
        const cname1_to = ReactDOM.findDOMNode(this.refs.cname1_to).value.trim();
        const cname1_from = ReactDOM.findDOMNode(this.refs.cname3_from).value.trim();
        const cname2_to = ReactDOM.findDOMNode(this.refs.cname2_to).value.trim();
        const cname2_from = ReactDOM.findDOMNode(this.refs.cname2_from).value.trim();
        const cname3_to = ReactDOM.findDOMNode(this.refs.cname3_to).value.trim();
        const cname3_from = ReactDOM.findDOMNode(this.refs.cname3_from).value.trim();
        const Expertise = ReactDOM.findDOMNode(this.refs.Expertise).value.trim();
        const Personality = ReactDOM.findDOMNode(this.refs.Personality).value.trim();
        const Languages = ReactDOM.findDOMNode(this.refs.Languages).value.trim();
        const Soft_skills = ReactDOM.findDOMNode(this.refs.Soft_skills).value.trim();
        const Hobbies = ReactDOM.findDOMNode(this.refs.Hobbies).value.trim();
        const Links = ReactDOM.findDOMNode(this.refs.Links).value.trim();

        const image = ReactDOM.findDOMNode(this.refs.image).value.trim();





        Tasks10.insert({
            name,
            contact,
            tel_number,
            email,
            address,
            objective,
            birth_date,
            school_name,
            school_major,
            from_school,
            to_school,
            college_name,
            college_major,
            from_college,
            to_college,
            university_name,
            university_major,
            from_university,
            to_university,
            cname1,
            post1,
            desc1,
            cname2,
            post2,
            desc2,
            cname3,
            post3,
            desc3,
            cname1_to,
            cname1_from,
            cname2_to,
            cname2_from,
            cname3_to,
            cname3_from,
            Expertise,
            Personality,
            Languages,
            Soft_skills,
            Hobbies,
            Links,
            image,

            createdAt: new Date(), // current time
        });
        // Clear form
        ReactDOM.findDOMNode(this.refs.name).value="";
        ReactDOM.findDOMNode(this.refs.contact_number).value="";
        ReactDOM.findDOMNode(this.refs.email).value="";
        ReactDOM.findDOMNode(this.refs.address).value="";
        ReactDOM.findDOMNode(this.refs.objective).value="";
        ReactDOM.findDOMNode(this.refs.birth_date).value="";
        ReactDOM.findDOMNode(this.refs.school_name).value="";
        ReactDOM.findDOMNode(this.refs.school_major).value="";
        ReactDOM.findDOMNode(this.refs.from_school).value="";
        ReactDOM.findDOMNode(this.refs.to_school).value="";
        ReactDOM.findDOMNode(this.refs.college_name).value="";
        ReactDOM.findDOMNode(this.refs.college_major).value="";
        ReactDOM.findDOMNode(this.refs.from_college).value="";
        ReactDOM.findDOMNode(this.refs.to_college).value="";
        ReactDOM.findDOMNode(this.refs.university_name).value="";
        ReactDOM.findDOMNode(this.refs.university_major).value="";
        ReactDOM.findDOMNode(this.refs.from_university).value="";
        ReactDOM.findDOMNode(this.refs.to_university).value="";

        ReactDOM.findDOMNode(this.refs.cname1).value="";
        ReactDOM.findDOMNode(this.refs.post1).value="";
        ReactDOM.findDOMNode(this.refs.desc1).value="";
        ReactDOM.findDOMNode(this.refs.cname2).value="";
        ReactDOM.findDOMNode(this.refs.post2).value="";
        ReactDOM.findDOMNode(this.refs.desc2).value="";
        ReactDOM.findDOMNode(this.refs.cname3).value="";
        ReactDOM.findDOMNode(this.refs.post3).value="";
        ReactDOM.findDOMNode(this.refs.desc3).value="";
        ReactDOM.findDOMNode(this.refs.cname1_to).value="";
        ReactDOM.findDOMNode(this.refs.cname3_from).value="";
        ReactDOM.findDOMNode(this.refs.cname2_to).value="";
        ReactDOM.findDOMNode(this.refs.cname2_from).value="";
        ReactDOM.findDOMNode(this.refs.cname3_to).value="";
        ReactDOM.findDOMNode(this.refs.cname3_from).value="";
        ReactDOM.findDOMNode(this.refs.Expertise).value="";
        ReactDOM.findDOMNode(this.refs.Personality).value="";
        ReactDOM.findDOMNode(this.refs.Languages).value="";
        ReactDOM.findDOMNode(this.refs.Soft_skills).value="";
        ReactDOM.findDOMNode(this.refs.Hobbies).value="";
        ReactDOM.findDOMNode(this.refs.Links).value="";


    }
    _handleImageChange(e) {
        e.preventDefault();

        let reader = new FileReader();
        let file = e.target.files[0];

        reader.onloadend = () => {
            this.setState({
                file: file,
                imagePreviewUrl: reader.result
            });
        }

        reader.readAsDataURL(file)
    }
    constructor(props) {
        super(props);
        this.state = {
            Dob: '',
            file: '',
            error: '',
            error2: '',
            error3: '',
            error4: '',
            Address: '',
            objective:'',
            School_name:'',
            smajor:'',
            sFrom:'',
            sTo:'',
            College_name:'',
            cmajor:'',
            cFrom:'',
            Uni_name:'',
            uFrom:'',
            uTo:'',
            Cname1:'',
            post1:'',
            Desc1:'',
            Cname2:'',
            post2:'',
            Desc2:'',
            Cname3:'',
            post3:'',
            Desc3:'',
            Experties:'',
            Personality:'',
            Languages:'',
            Software_Skills:'',
            Hobbies:'',
            Links:'',
            Image:'',

            imagePreviewUrl: '',};
        this.state={UserName:''};
        this.state={Contact:''};
        this.state={Tel:''};
        this.state={Email:''};
        this._handleImageChange = this._handleImageChange.bind(this);
        this._handleSubmit = this._handleSubmit.bind(this);
        this.handleChangeUserName = this.handleChangeUserName.bind(this);
        this.handleChangeContact = this.handleChangeContact.bind(this);
        this.handleChangeTel = this.handleChangeTel.bind(this);
        this.handleChangeEmail = this.handleChangeEmail.bind(this);
        this.handleAddress = this.handleAddress.bind(this);
        this.handleDob = this.handleDob.bind(this);
        this.handleAddress = this.handleAddress.bind(this);
        this.handleObjective = this.handleObjective.bind(this);
        this.handleSchool_name = this.handleSchool_name.bind(this);
        this.handlesmajor = this.handlesmajor.bind(this);
        this.handlesFrom = this.handlesFrom.bind(this);
        this.handlesTo = this.handlesTo.bind(this);
        this.handleCollege_name = this.handleCollege_name.bind(this);
        this.handlecmajor = this.handlecmajor.bind(this);
        this.handlecFrom = this.handlecFrom.bind(this);
        this.handlecTo = this.handlecTo.bind(this);
        this.handleUni_name = this.handleUni_name.bind(this);
        this.handleumajor = this.handleumajor.bind(this);
        this.handleuFrom = this.handleuFrom.bind(this);
        this.handleuTo = this.handleuTo.bind(this);
        this.handleCname1 = this.handleCname1.bind(this);
        this.handlepost1 = this.handlepost1.bind(this);
        this.handleDesc1 = this.handleDesc1.bind(this);
        this.handlePersonality = this.handlePersonality.bind(this);
        this.handleLanguages = this.handleLanguages.bind(this);
        this.handleExperties = this.handleExperties.bind(this);
        this.handleSoftware_skills = this.handleSoftware_skills.bind(this);
        this.handleHobbies = this.handleHobbies.bind(this);
        this.handleLinks = this.handleLinks.bind(this);

    }

    _handleSubmit(e) {
        //  e.preventDefault();
        //this.state.imagePreviewUrl = image
        // TODO: do something with -> this.state.file

    }

    handleChangeUserName(e){

        console.log(e.target.value.match("^[a-zA-Z ]*$"));
        if(e.target.value.match("^[a-zA-Z ]*$")!=null)
        {
            this.setState({UserName: e.target.value});
            this.setState({error:""});
        }
        else
        {this.setState({error:"Only alphabets allowed!"});
            console.log("error"+this.state.error);}
    }

    handleAddress(e){
        this.setState({Address: e.target.value});
    }
    handleObjective(e){
        this.setState({objective: e.target.value});
        console.log("objective here"+this.state.objective);
    }
    handleDob(e){
        this.setState({Dob: e.target.value});
        console.log("Dob here"+this.state.Dob);

    }
    handleSchool_name(e){
        this.setState({School_name: e.target.value});

        console.log("School_name here: "+this.state.School_name);
    }
    handlesmajor(e){
        this.setState({smajor: e.target.value});
        console.log("smajor here: "+this.state.smajor);

    }
    handlesFrom(e){
        this.setState({sFrom: e.target.value});
        console.log("sFrom here: "+this.state.sFrom);
    }
    handlesTo(e){
        this.setState({sTo: e.target.value});
        console.log("sTo here: "+this.state.sTo);
    }
    handleCollege_name(e){
        this.setState({College_name: e.target.value});
        console.log("College_name here: "+this.state.College_name);
    }
    handlecmajor(e){
        this.setState({cmajor: e.target.value});
        console.log("cmajor here: "+this.state.cmajor);
    }
    handlecFrom(e){
        this.setState({cFrom: e.target.value});
        console.log("cFrom here: "+this.state.cFrom);
    }
    handlecTo(e){
        this.setState({cTo: e.target.value});
        console.log("cTo here: "+this.state.cTo);
    }
    handleUni_name(e){
        this.setState({Uni_name: e.target.value});
        console.log("Uni_name here: "+this.state.Uni_name);
    }
    handleumajor(e){
        this.setState({umajor: e.target.value});
        console.log("umajor here: "+this.state.umajor);
    }
    handleuFrom(e){
        this.setState({uFrom: e.target.value});
        console.log("uFrom here: "+this.state.uFrom);
    }
    handleuTo(e){
        this.setState({uTo: e.target.value});
        console.log("uTo here: "+this.state.uTo);
    }
    handleCname1(e){
        this.setState({Cname1: e.target.value});
        console.log("Cname1 here: "+this.state.Cname1);
    }
    handlepost1(e){
        this.setState({post1: e.target.value});
        console.log("post1 here: "+this.state.post1);
    }
    handleDesc1(e){
        this.setState({Desc1: e.target.value});
        console.log("Desc1 here: "+this.state.Desc1);
    }
    handleCname2(e){
        this.setState({Cname2: e.target.value});
    }
    handlepost2(e){
        this.setState({post2: e.target.value});
    }
    handleDesc2(e){
        this.setState({Desc2: e.target.value});
    }

    handleCname3(e){
        this.setState({Cname3: e.target.value});
    }
    handlepost3(e){
        this.setState({post3: e.target.value});
    }
    handleDesc3(e){
        this.setState({Desc3: e.target.value});
    }
    handleExperties(e){
        this.setState({Experties: e.target.value});
        console.log("Experties here: "+this.state.Experties);
    }
    handlePersonality(e){
        this.setState({Personality: e.target.value});
        console.log("Personality here: "+this.state.Personality);
    }
    handleLanguages(e){
        this.setState({Languages: e.target.value});
        console.log("Languages here: "+this.state.Languages);
    }
    handleSoftware_skills(e){
        this.setState({Software_skills: e.target.value});
        console.log("Software_skills here: "+this.state.Software_skills);
    }
    handleHobbies(e){
        this.setState({Hobbies: e.target.value});
        console.log("Hobbies here: "+this.state.Hobbies);
    }
    handleLinks(e){
        this.setState({Links: e.target.value});
        console.log("Links here: "+this.state.Links);
    }






    handleChangeContact(e){
        console.log(e.target.value.match("^[0-9]*$"));
        if(e.target.value.match("^[0-9]*$")!=null){
            this.setState({Contact: e.target.value});
            this.setState({error2:""});
        }
        else
        {this.setState({error2:"Only numbers allowed!"});
            console.log("error2"+this.state.error2);}
    }

    handleChangeEmail(e){
        //let re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        // let re = "/[A-Z0-9._%+-]+@[A-Z0-9.-]+.[A-Z]{2,4}/igm";
        console.log("here->"+e.target.value.match(/^[a-zA-Z0-9]+@[a-zA-Z0-9]+\.[A-Za-z]+$/));
        if(/^[A-Z0-9'.1234z_%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test(e.target.value)){
            this.setState({Email: e.target.value});
            this.setState({error4:""});
        }
        else
        {this.setState({error4:"Invalid Email!"});
            console.log("error4"+this.state.error4);}
    }

    handleChangeTel(e){
        console.log(e.target.value.match("^[0-9]*$"));
        if(e.target.value.match("^[0-9]*$")!=null){
            this.setState({Tel: e.target.value});
            this.setState({error3:""});
        }
        else
        {this.setState({error3:"Only numbers allowed!"});
            console.log("error2"+this.state.error2);}
    }
    renderTasks() {
        return this.props.tasks.map((task) => (
            <Cv key={task._id} task={task}/>
        ));
    }



    renderTasks2() {
        return this.props.tasks.map((task) => (
            <Cv2 key={task._id} task={task}/>
        ));
    }
    showIt() {
        document.getElementsByClassName("div1").style.visibility = "visible";
        setTimeout("showIt()", 1000); // after 1 sec
    }


    render()
    {
        let {imagePreviewUrl} = this.state;
        let $imagePreview = null;
        if (imagePreviewUrl) {

            $imagePreview = (<img src={imagePreviewUrl} />);

        }
        let {error} = this.state;
        if (error==null) {

            console.log("herhekjrhk");

        }
        const huzaifa = imagePreviewUrl;


        return (
            <div className="hamza container">
                <div className="text-center">
                    <img src="images/cv.png" className="rounded" alt="..." height="10%" width="10%"/>
                </div>
                <div className="text-center">
                    <h4 className="text-center c2">CV MAKER</h4>
                </div>

                <div className="hamza21">
                    <div className="text-center">
                        <img className="rounded" width="45%" height="45%" src ="images/v23.png" />
                    </div>
                    <div className="center">
                        <div className="row hamza3row">
                            <div className="col-sm-6 text-center" >
                                <button type="button" className="btn btn-primary btn-lg " data-toggle="modal" data-target="#myModal">
                                    Information
                                </button></div>

                            <div className="col-sm-6 text-center">
                                <button type="button" className=" btn btn-primary btn-lg " data-toggle="modal" data-target="#myModal2">
                                    Show CV
                                </button></div></div>


                        <div className="modal fade" id="myModal" role="dialog">
                            <div className="modal-dialog">

                                <div className="modal-content">
                                    <div className="modal-header">
                                        <button type="button" className="close" data-dismiss="modal">&times;</button>
                                        <h4 className="modal-title">Add your Information</h4>
                                    </div>
                                    <div className="modal-body modalC1">
                                        <form>
                                            <div className="form-group">
                                                <span className="error">*</span><label>Name: </label><span className="error">{this.state.error}</span>
                                                <input type="text" className="form-control" ref="name" value={this.state.UserName}
                                                       placeholder="Enter Name " onChange={this.handleChangeUserName} />
                                            </div>
                                            <div className="form-group">
                                                <span className="error">*</span><label>Date of Birth</label>
                                                <input type="date" className="form-control" onChange={this.handleDob} id="exampleInputDOB1" placeholder="Date of Birth" ref="birth_date"/>
                                            </div>

                                            {/*<div className="form-group">
                                             <label>Date of Birth:</label>
                                             <label>Day: </label>

                                             <select className="form-control" id="sel1">
                                             <option>1</option>
                                             <option>2</option>
                                             <option>3</option>
                                             <option>4</option>
                                             <option>5</option>
                                             <option>6</option>
                                             <option>7</option>
                                             <option>8</option>
                                             <option>9</option>
                                             <option>10</option>
                                             <option>11</option>
                                             <option>12</option>
                                             <option>13</option>
                                             <option>14</option>
                                             <option>15</option>
                                             <option>16</option>
                                             <option>17</option>
                                             <option>18</option>
                                             <option>18</option>
                                             <option>20</option>
                                             <option>21</option>
                                             <option>22</option>
                                             <option>23</option>
                                             <option>24</option>
                                             <option>25</option>
                                             <option>26</option>
                                             <option>27</option>
                                             <option>28</option>
                                             <option>29</option>
                                             <option>30</option>
                                             <option>31</option>
                                             </select>
                                             <label>Month:  </label>
                                             <select className="form-control" id="sel1">
                                             <option>1</option>
                                             <option>2</option>
                                             <option>3</option>
                                             <option>4</option>
                                             <option>5</option>
                                             <option>6</option>
                                             <option>7</option>
                                             <option>8</option>
                                             <option>9</option>
                                             <option>10</option>
                                             <option>11</option>
                                             <option>12</option>
                                             </select>
                                             <label>Year:  </label>
                                             <select className="form-control" id="sel1">
                                             <option>1960</option>
                                             <option>1961</option>
                                             <option>1962</option>
                                             <option>1963</option>
                                             <option>1964</option>
                                             <option>1965</option>
                                             <option>1966</option>
                                             <option>1967</option>
                                             <option>1968</option>
                                             <option>1969</option>
                                             <option>1970</option>
                                             <option>1971</option>
                                             <option>1972</option>
                                             <option>1973</option>
                                             <option>1974</option>
                                             <option>1975</option>
                                             <option>1976</option>
                                             <option>1977</option>
                                             <option>1978</option>
                                             <option>1979</option>
                                             <option>1980</option>
                                             <option>1981</option>
                                             <option>1982</option>
                                             <option>1983</option>
                                             <option>1984</option>
                                             <option>1985</option>
                                             <option>1986</option>
                                             <option>1987</option>
                                             <option>1988</option>
                                             <option>1989</option>
                                             <option>1990</option>
                                             <option>1991</option>
                                             <option>1992</option>
                                             <option>1993</option>
                                             <option>1994</option>
                                             <option>1995</option>
                                             <option>1996</option>
                                             <option>1997</option>
                                             <option>1998</option>
                                             <option>1999</option>
                                             <option>2001</option>
                                             <option>2002</option>
                                             <option>2003</option>
                                             <option>2004</option>
                                             <option>2005</option>
                                             <option>2006</option>
                                             <option>2007</option>
                                             <option>2008</option>
                                             <option>2009</option>
                                             <option>2010</option>
                                             <option>2011</option>
                                             <option>2012</option>
                                             <option>2013</option>
                                             <option>2014</option>
                                             <option>2015</option>
                                             <option>2016</option>
                                             <option>2017</option>
                                             </select>
                                             </div>*/}
                                            <div className="form-group">
                                                <span className="error">*</span><label>Contact No:</label><span className="error">{this.state.error2}</span>
                                                <input type="text" className="form-control" required="required" ref="contact_number" value={this.state.Contact} onChange={this.handleChangeContact}
                                                       placeholder="Enter contact No"/>
                                            </div>
                                            <div className="form-group">
                                                <span className="error">*</span><label>Tel No:</label><span className="error">{this.state.error3}</span>
                                                <input type="text" className="form-control" ref="tel_number" onChange={this.handleChangeTel}
                                                       placeholder="Enter Tel No"/>
                                            </div>
                                            <div className="form-group">
                                                <span className="error">*</span><label>Enter Email:</label><span className="error">{this.state.error4}</span>
                                                <input type="text" className="form-control" ref="email" onChange={this.handleChangeEmail}
                                                       placeholder="Enter Email"/>
                                            </div>
                                            <div className="form-group">
                                                <span className="error">*</span><label>Enter Address:</label>
                                                <input type="text" className="form-control" ref="address" onChange={this.handleAddress}
                                                       placeholder="Enter Address"/>
                                            </div>
                                            <div className="form-group">
                                                <span className="error">*</span> <label>Enter Objective:</label>
                                                <textarea type="comment" className="form-control" ref="objective" onChange={this.handleObjective}
                                                          placeholder="Enter your Objective"/>
                                            </div>
                                            <div className="form-group">
                                                <span className="error">*</span>  <label>School Name:</label>
                                                <input type="comment" className="form-control" ref="school_name" onChange={this.handleSchool_name}
                                                       placeholder="School Name"/>
                                            </div>
                                            <div className="form-group">
                                                <span className="error">*</span><label>Major:</label>
                                                <input type="comment" className="form-control" ref="school_major" onChange={this.handlesmajor}
                                                       placeholder="Major Subject"/>
                                            </div>
                                            <div className="form-group form-inline">
                                                <span className="error">*</span> <label>From:</label>
                                                <select className="form-control" id="sel1" ref="from_school" onChange={this.handlesFrom}>
                                                    <option>1960</option>
                                                    <option>1961</option>
                                                    <option>1962</option>
                                                    <option>1963</option>
                                                    <option>1964</option>
                                                    <option>1965</option>
                                                    <option>1966</option>
                                                    <option>1967</option>
                                                    <option>1968</option>
                                                    <option>1969</option>
                                                    <option>1970</option>
                                                    <option>1971</option>
                                                    <option>1972</option>
                                                    <option>1973</option>
                                                    <option>1974</option>
                                                    <option>1975</option>
                                                    <option>1976</option>
                                                    <option>1977</option>
                                                    <option>1978</option>
                                                    <option>1979</option>
                                                    <option>1980</option>
                                                    <option>1981</option>
                                                    <option>1982</option>
                                                    <option>1983</option>
                                                    <option>1984</option>
                                                    <option>1985</option>
                                                    <option>1986</option>
                                                    <option>1987</option>
                                                    <option>1988</option>
                                                    <option>1989</option>
                                                    <option>1990</option>
                                                    <option>1991</option>
                                                    <option>1992</option>
                                                    <option>1993</option>
                                                    <option>1994</option>
                                                    <option>1995</option>
                                                    <option>1996</option>
                                                    <option>1997</option>
                                                    <option>1998</option>
                                                    <option>1999</option>
                                                    <option>2001</option>
                                                    <option>2002</option>
                                                    <option>2003</option>
                                                    <option>2004</option>
                                                    <option>2005</option>
                                                    <option>2006</option>
                                                    <option>2007</option>
                                                    <option>2008</option>
                                                    <option>2009</option>
                                                    <option>2010</option>
                                                    <option>2011</option>
                                                    <option>2012</option>
                                                    <option>2013</option>
                                                    <option>2014</option>
                                                    <option>2015</option>
                                                    <option>2016</option>
                                                    <option>2017</option>
                                                </select>
                                                <span className="error">*</span><label> To:</label>
                                                <select className="form-control" id="sel1" ref ="to_school" onChange={this.handlesTo}>
                                                    <option>1960</option>
                                                    <option>1961</option>
                                                    <option>1962</option>
                                                    <option>1963</option>
                                                    <option>1964</option>
                                                    <option>1965</option>
                                                    <option>1966</option>
                                                    <option>1967</option>
                                                    <option>1968</option>
                                                    <option>1969</option>
                                                    <option>1970</option>
                                                    <option>1971</option>
                                                    <option>1972</option>
                                                    <option>1973</option>
                                                    <option>1974</option>
                                                    <option>1975</option>
                                                    <option>1976</option>
                                                    <option>1977</option>
                                                    <option>1978</option>
                                                    <option>1979</option>
                                                    <option>1980</option>
                                                    <option>1981</option>
                                                    <option>1982</option>
                                                    <option>1983</option>
                                                    <option>1984</option>
                                                    <option>1985</option>
                                                    <option>1986</option>
                                                    <option>1987</option>
                                                    <option>1988</option>
                                                    <option>1989</option>
                                                    <option>1990</option>
                                                    <option>1991</option>
                                                    <option>1992</option>
                                                    <option>1993</option>
                                                    <option>1994</option>
                                                    <option>1995</option>
                                                    <option>1996</option>
                                                    <option>1997</option>
                                                    <option>1998</option>
                                                    <option>1999</option>
                                                    <option>2001</option>
                                                    <option>2002</option>
                                                    <option>2003</option>
                                                    <option>2004</option>
                                                    <option>2005</option>
                                                    <option>2006</option>
                                                    <option>2007</option>
                                                    <option>2008</option>
                                                    <option>2009</option>
                                                    <option>2010</option>
                                                    <option>2011</option>
                                                    <option>2012</option>
                                                    <option>2013</option>
                                                    <option>2014</option>
                                                    <option>2015</option>
                                                    <option>2016</option>
                                                    <option>2017</option>
                                                </select>
                                            </div>
                                            <div className="form-group">

                                                <span className="error">*</span><label>College Name:</label>
                                                <input type="comment" className="form-control" ref="college_name" onChange={this.handleCollege_name}
                                                       placeholder="College Name"/>
                                            </div>
                                            <div className="form-group">
                                                <span className="error">*</span> <label>Major:</label>
                                                <input type="comment" className="form-control" ref="college_major" onChange={this.handlecmajor}
                                                       placeholder="Major Subject"/>
                                            </div>
                                            <div className="form-group form-inline">
                                                <span className="error">*</span> <label>From:</label>
                                                <select className="form-control" id="sel1" ref="from_college" onChange={this.handlecFrom}>
                                                    <option>1960</option>
                                                    <option>1961</option>
                                                    <option>1962</option>
                                                    <option>1963</option>
                                                    <option>1964</option>
                                                    <option>1965</option>
                                                    <option>1966</option>
                                                    <option>1967</option>
                                                    <option>1968</option>
                                                    <option>1969</option>
                                                    <option>1970</option>
                                                    <option>1971</option>
                                                    <option>1972</option>
                                                    <option>1973</option>
                                                    <option>1974</option>
                                                    <option>1975</option>
                                                    <option>1976</option>
                                                    <option>1977</option>
                                                    <option>1978</option>
                                                    <option>1979</option>
                                                    <option>1980</option>
                                                    <option>1981</option>
                                                    <option>1982</option>
                                                    <option>1983</option>
                                                    <option>1984</option>
                                                    <option>1985</option>
                                                    <option>1986</option>
                                                    <option>1987</option>
                                                    <option>1988</option>
                                                    <option>1989</option>
                                                    <option>1990</option>
                                                    <option>1991</option>
                                                    <option>1992</option>
                                                    <option>1993</option>
                                                    <option>1994</option>
                                                    <option>1995</option>
                                                    <option>1996</option>
                                                    <option>1997</option>
                                                    <option>1998</option>
                                                    <option>1999</option>
                                                    <option>2001</option>
                                                    <option>2002</option>
                                                    <option>2003</option>
                                                    <option>2004</option>
                                                    <option>2005</option>
                                                    <option>2006</option>
                                                    <option>2007</option>
                                                    <option>2008</option>
                                                    <option>2009</option>
                                                    <option>2010</option>
                                                    <option>2011</option>
                                                    <option>2012</option>
                                                    <option>2013</option>
                                                    <option>2014</option>
                                                    <option>2015</option>
                                                    <option>2016</option>
                                                    <option>2017</option>
                                                </select>
                                                <span className="error">*</span><label> To:</label>
                                                <select className="form-control" id="sel1" ref="to_college" onChange={this.handlecTo}>
                                                    <option>1960</option>
                                                    <option>1961</option>
                                                    <option>1962</option>
                                                    <option>1963</option>
                                                    <option>1964</option>
                                                    <option>1965</option>
                                                    <option>1966</option>
                                                    <option>1967</option>
                                                    <option>1968</option>
                                                    <option>1969</option>
                                                    <option>1970</option>
                                                    <option>1971</option>
                                                    <option>1972</option>
                                                    <option>1973</option>
                                                    <option>1974</option>
                                                    <option>1975</option>
                                                    <option>1976</option>
                                                    <option>1977</option>
                                                    <option>1978</option>
                                                    <option>1979</option>
                                                    <option>1980</option>
                                                    <option>1981</option>
                                                    <option>1982</option>
                                                    <option>1983</option>
                                                    <option>1984</option>
                                                    <option>1985</option>
                                                    <option>1986</option>
                                                    <option>1987</option>
                                                    <option>1988</option>
                                                    <option>1989</option>
                                                    <option>1990</option>
                                                    <option>1991</option>
                                                    <option>1992</option>
                                                    <option>1993</option>
                                                    <option>1994</option>
                                                    <option>1995</option>
                                                    <option>1996</option>
                                                    <option>1997</option>
                                                    <option>1998</option>
                                                    <option>1999</option>
                                                    <option>2001</option>
                                                    <option>2002</option>
                                                    <option>2003</option>
                                                    <option>2004</option>
                                                    <option>2005</option>
                                                    <option>2006</option>
                                                    <option>2007</option>
                                                    <option>2008</option>
                                                    <option>2009</option>
                                                    <option>2010</option>
                                                    <option>2011</option>
                                                    <option>2012</option>
                                                    <option>2013</option>
                                                    <option>2014</option>
                                                    <option>2015</option>
                                                    <option>2016</option>
                                                    <option>2017</option>
                                                </select>
                                            </div>

                                            <div className="form-group">
                                                <span className="error">*</span>  <label>University Name:</label>
                                                <input type="comment" className="form-control" ref="university_name" onChange={this.handleUni_name}
                                                       placeholder="University Name"/>
                                            </div>
                                            <div className="form-group">
                                                <span className="error">*</span>   <label>Major:</label>
                                                <input type="comment" className="form-control" ref="university_major" onChange={this.handleumajor}
                                                       placeholder="Major subject"/>
                                            </div>
                                            <div className="form-group form-inline">
                                                <span className="error">*</span> <label>From:</label>
                                                <select className="form-control" id="sel1" ref="from_university" onChange={this.handleuFrom}>
                                                    <option>1960</option>
                                                    <option>1961</option>
                                                    <option>1962</option>
                                                    <option>1963</option>
                                                    <option>1964</option>
                                                    <option>1965</option>
                                                    <option>1966</option>
                                                    <option>1967</option>
                                                    <option>1968</option>
                                                    <option>1969</option>
                                                    <option>1970</option>
                                                    <option>1971</option>
                                                    <option>1972</option>
                                                    <option>1973</option>
                                                    <option>1974</option>
                                                    <option>1975</option>
                                                    <option>1976</option>
                                                    <option>1977</option>
                                                    <option>1978</option>
                                                    <option>1979</option>
                                                    <option>1980</option>
                                                    <option>1981</option>
                                                    <option>1982</option>
                                                    <option>1983</option>
                                                    <option>1984</option>
                                                    <option>1985</option>
                                                    <option>1986</option>
                                                    <option>1987</option>
                                                    <option>1988</option>
                                                    <option>1989</option>
                                                    <option>1990</option>
                                                    <option>1991</option>
                                                    <option>1992</option>
                                                    <option>1993</option>
                                                    <option>1994</option>
                                                    <option>1995</option>
                                                    <option>1996</option>
                                                    <option>1997</option>
                                                    <option>1998</option>
                                                    <option>1999</option>
                                                    <option>2001</option>
                                                    <option>2002</option>
                                                    <option>2003</option>
                                                    <option>2004</option>
                                                    <option>2005</option>
                                                    <option>2006</option>
                                                    <option>2007</option>
                                                    <option>2008</option>
                                                    <option>2009</option>
                                                    <option>2010</option>
                                                    <option>2011</option>
                                                    <option>2012</option>
                                                    <option>2013</option>
                                                    <option>2014</option>
                                                    <option>2015</option>
                                                    <option>2016</option>
                                                    <option>2017</option>
                                                </select>
                                                <span className="error">*</span>  <label> To:</label>
                                                <select className="form-control" id="sel1" ref="to_university" onChange={this.handleuTo}>
                                                    <option>1960</option>
                                                    <option>1961</option>
                                                    <option>1962</option>
                                                    <option>1963</option>
                                                    <option>1964</option>
                                                    <option>1965</option>
                                                    <option>1966</option>
                                                    <option>1967</option>
                                                    <option>1968</option>
                                                    <option>1969</option>
                                                    <option>1970</option>
                                                    <option>1971</option>
                                                    <option>1972</option>
                                                    <option>1973</option>
                                                    <option>1974</option>
                                                    <option>1975</option>
                                                    <option>1976</option>
                                                    <option>1977</option>
                                                    <option>1978</option>
                                                    <option>1979</option>
                                                    <option>1980</option>
                                                    <option>1981</option>
                                                    <option>1982</option>
                                                    <option>1983</option>
                                                    <option>1984</option>
                                                    <option>1985</option>
                                                    <option>1986</option>
                                                    <option>1987</option>
                                                    <option>1988</option>
                                                    <option>1989</option>
                                                    <option>1990</option>
                                                    <option>1991</option>
                                                    <option>1992</option>
                                                    <option>1993</option>
                                                    <option>1994</option>
                                                    <option>1995</option>
                                                    <option>1996</option>
                                                    <option>1997</option>
                                                    <option>1998</option>
                                                    <option>1999</option>
                                                    <option>2001</option>
                                                    <option>2002</option>
                                                    <option>2003</option>
                                                    <option>2004</option>
                                                    <option>2005</option>
                                                    <option>2006</option>
                                                    <option>2007</option>
                                                    <option>2008</option>
                                                    <option>2009</option>
                                                    <option>2010</option>
                                                    <option>2011</option>
                                                    <option>2012</option>
                                                    <option>2013</option>
                                                    <option>2014</option>
                                                    <option>2015</option>
                                                    <option>2016</option>
                                                    <option>2017</option>
                                                </select>
                                            </div>
                                            <div className="form-group">
                                                <span className="error">*</span><label>Company Name 1:</label>
                                                <input type="text" className="form-control" ref="cname1" onChange={this.handleCname1}
                                                       placeholder="Enter Company Name"/>
                                            </div>
                                            <div className="form-group">
                                                <span className="error">*</span><label>Post:</label>
                                                <input type="text" className="form-control" ref="post1" onChange={this.handlepost1}
                                                       placeholder="Enter Post"/>
                                            </div>
                                            <div className="form-group">
                                                <span className="error">*</span>   <label>Description:</label>
                                                <input type="text" className="form-control" ref="desc1" onChange={this.handleDesc1}
                                                       placeholder="Description"/>
                                            </div>
                                            <div className="form-group form-inline">
                                                <span className="error">*</span> <label>From:</label>
                                                <select className="form-control" id="sel1" ref="cname1_from" onChange={this.handlecnameFrom}>
                                                    <option>1960</option>
                                                    <option>1961</option>
                                                    <option>1962</option>
                                                    <option>1963</option>
                                                    <option>1964</option>
                                                    <option>1965</option>
                                                    <option>1966</option>
                                                    <option>1967</option>
                                                    <option>1968</option>
                                                    <option>1969</option>
                                                    <option>1970</option>
                                                    <option>1971</option>
                                                    <option>1972</option>
                                                    <option>1973</option>
                                                    <option>1974</option>
                                                    <option>1975</option>
                                                    <option>1976</option>
                                                    <option>1977</option>
                                                    <option>1978</option>
                                                    <option>1979</option>
                                                    <option>1980</option>
                                                    <option>1981</option>
                                                    <option>1982</option>
                                                    <option>1983</option>
                                                    <option>1984</option>
                                                    <option>1985</option>
                                                    <option>1986</option>
                                                    <option>1987</option>
                                                    <option>1988</option>
                                                    <option>1989</option>
                                                    <option>1990</option>
                                                    <option>1991</option>
                                                    <option>1992</option>
                                                    <option>1993</option>
                                                    <option>1994</option>
                                                    <option>1995</option>
                                                    <option>1996</option>
                                                    <option>1997</option>
                                                    <option>1998</option>
                                                    <option>1999</option>
                                                    <option>2001</option>
                                                    <option>2002</option>
                                                    <option>2003</option>
                                                    <option>2004</option>
                                                    <option>2005</option>
                                                    <option>2006</option>
                                                    <option>2007</option>
                                                    <option>2008</option>
                                                    <option>2009</option>
                                                    <option>2010</option>
                                                    <option>2011</option>
                                                    <option>2012</option>
                                                    <option>2013</option>
                                                    <option>2014</option>
                                                    <option>2015</option>
                                                    <option>2016</option>
                                                    <option>2017</option>
                                                </select>
                                                <span className="error">*</span> <label> To:</label>
                                                <select className="form-control" id="sel1" ref="cname1_to" onChange={this.handlecnameTo}>
                                                    <option>1960</option>
                                                    <option>1961</option>
                                                    <option>1962</option>
                                                    <option>1963</option>
                                                    <option>1964</option>
                                                    <option>1965</option>
                                                    <option>1966</option>
                                                    <option>1967</option>
                                                    <option>1968</option>
                                                    <option>1969</option>
                                                    <option>1970</option>
                                                    <option>1971</option>
                                                    <option>1972</option>
                                                    <option>1973</option>
                                                    <option>1974</option>
                                                    <option>1975</option>
                                                    <option>1976</option>
                                                    <option>1977</option>
                                                    <option>1978</option>
                                                    <option>1979</option>
                                                    <option>1980</option>
                                                    <option>1981</option>
                                                    <option>1982</option>
                                                    <option>1983</option>
                                                    <option>1984</option>
                                                    <option>1985</option>
                                                    <option>1986</option>
                                                    <option>1987</option>
                                                    <option>1988</option>
                                                    <option>1989</option>
                                                    <option>1990</option>
                                                    <option>1991</option>
                                                    <option>1992</option>
                                                    <option>1993</option>
                                                    <option>1994</option>
                                                    <option>1995</option>
                                                    <option>1996</option>
                                                    <option>1997</option>
                                                    <option>1998</option>
                                                    <option>1999</option>
                                                    <option>2001</option>
                                                    <option>2002</option>
                                                    <option>2003</option>
                                                    <option>2004</option>
                                                    <option>2005</option>
                                                    <option>2006</option>
                                                    <option>2007</option>
                                                    <option>2008</option>
                                                    <option>2009</option>
                                                    <option>2010</option>
                                                    <option>2011</option>
                                                    <option>2012</option>
                                                    <option>2013</option>
                                                    <option>2014</option>
                                                    <option>2015</option>
                                                    <option>2016</option>
                                                    <option>2017</option>
                                                </select>
                                            </div>
                                            <div className="form-group">
                                                <label>Company Name 2:</label>
                                                <input type="text" className="form-control" ref="cname2"
                                                       placeholder="Company name"/>
                                            </div>
                                            <div className="form-group">
                                                <label>Post:</label>
                                                <input type="text" className="form-control" ref="post2"
                                                       placeholder="Enter Post"/>
                                            </div>
                                            <div className="form-group">
                                                <label>Description:</label>
                                                <input type="text" className="form-control" ref="desc2"
                                                       placeholder="Description"/>
                                            </div>
                                            <div className="form-group form-inline">
                                                <label>From:</label>
                                                <select className="form-control" id="sel1" ref="cname2_from">
                                                    <option>1960</option>
                                                    <option>1961</option>
                                                    <option>1962</option>
                                                    <option>1963</option>
                                                    <option>1964</option>
                                                    <option>1965</option>
                                                    <option>1966</option>
                                                    <option>1967</option>
                                                    <option>1968</option>
                                                    <option>1969</option>
                                                    <option>1970</option>
                                                    <option>1971</option>
                                                    <option>1972</option>
                                                    <option>1973</option>
                                                    <option>1974</option>
                                                    <option>1975</option>
                                                    <option>1976</option>
                                                    <option>1977</option>
                                                    <option>1978</option>
                                                    <option>1979</option>
                                                    <option>1980</option>
                                                    <option>1981</option>
                                                    <option>1982</option>
                                                    <option>1983</option>
                                                    <option>1984</option>
                                                    <option>1985</option>
                                                    <option>1986</option>
                                                    <option>1987</option>
                                                    <option>1988</option>
                                                    <option>1989</option>
                                                    <option>1990</option>
                                                    <option>1991</option>
                                                    <option>1992</option>
                                                    <option>1993</option>
                                                    <option>1994</option>
                                                    <option>1995</option>
                                                    <option>1996</option>
                                                    <option>1997</option>
                                                    <option>1998</option>
                                                    <option>1999</option>
                                                    <option>2001</option>
                                                    <option>2002</option>
                                                    <option>2003</option>
                                                    <option>2004</option>
                                                    <option>2005</option>
                                                    <option>2006</option>
                                                    <option>2007</option>
                                                    <option>2008</option>
                                                    <option>2009</option>
                                                    <option>2010</option>
                                                    <option>2011</option>
                                                    <option>2012</option>
                                                    <option>2013</option>
                                                    <option>2014</option>
                                                    <option>2015</option>
                                                    <option>2016</option>
                                                    <option>2017</option>
                                                </select>
                                                <label> To:</label>
                                                <select className="form-control" id="sel1" ref="cname2_to">
                                                    <option>1960</option>
                                                    <option>1961</option>
                                                    <option>1962</option>
                                                    <option>1963</option>
                                                    <option>1964</option>
                                                    <option>1965</option>
                                                    <option>1966</option>
                                                    <option>1967</option>
                                                    <option>1968</option>
                                                    <option>1969</option>
                                                    <option>1970</option>
                                                    <option>1971</option>
                                                    <option>1972</option>
                                                    <option>1973</option>
                                                    <option>1974</option>
                                                    <option>1975</option>
                                                    <option>1976</option>
                                                    <option>1977</option>
                                                    <option>1978</option>
                                                    <option>1979</option>
                                                    <option>1980</option>
                                                    <option>1981</option>
                                                    <option>1982</option>
                                                    <option>1983</option>
                                                    <option>1984</option>
                                                    <option>1985</option>
                                                    <option>1986</option>
                                                    <option>1987</option>
                                                    <option>1988</option>
                                                    <option>1989</option>
                                                    <option>1990</option>
                                                    <option>1991</option>
                                                    <option>1992</option>
                                                    <option>1993</option>
                                                    <option>1994</option>
                                                    <option>1995</option>
                                                    <option>1996</option>
                                                    <option>1997</option>
                                                    <option>1998</option>
                                                    <option>1999</option>
                                                    <option>2001</option>
                                                    <option>2002</option>
                                                    <option>2003</option>
                                                    <option>2004</option>
                                                    <option>2005</option>
                                                    <option>2006</option>
                                                    <option>2007</option>
                                                    <option>2008</option>
                                                    <option>2009</option>
                                                    <option>2010</option>
                                                    <option>2011</option>
                                                    <option>2012</option>
                                                    <option>2013</option>
                                                    <option>2014</option>
                                                    <option>2015</option>
                                                    <option>2016</option>
                                                    <option>2017</option>
                                                </select>
                                            </div>
                                            <div className="form-group">
                                                <label>Company Name 3:</label>
                                                <input type="text" className="form-control" ref="cname3"
                                                       placeholder="Enter skills"/>
                                            </div>
                                            <div className="form-group">
                                                <label>Post:</label>
                                                <input type="text" className="form-control" ref="post3"
                                                       placeholder="Enter Post"/>
                                            </div>
                                            <div className="form-group">
                                                <label>Description:</label>
                                                <input type="text" className="form-control" ref="desc3"
                                                       placeholder="Desc"/>
                                            </div>
                                            <div className="form-group form-inline">
                                                <label>From:</label>
                                                <select className="form-control" id="sel1" ref="cname3_from">
                                                    <option>1960</option>
                                                    <option>1961</option>
                                                    <option>1962</option>
                                                    <option>1963</option>
                                                    <option>1964</option>
                                                    <option>1965</option>
                                                    <option>1966</option>
                                                    <option>1967</option>
                                                    <option>1968</option>
                                                    <option>1969</option>
                                                    <option>1970</option>
                                                    <option>1971</option>
                                                    <option>1972</option>
                                                    <option>1973</option>
                                                    <option>1974</option>
                                                    <option>1975</option>
                                                    <option>1976</option>
                                                    <option>1977</option>
                                                    <option>1978</option>
                                                    <option>1979</option>
                                                    <option>1980</option>
                                                    <option>1981</option>
                                                    <option>1982</option>
                                                    <option>1983</option>
                                                    <option>1984</option>
                                                    <option>1985</option>
                                                    <option>1986</option>
                                                    <option>1987</option>
                                                    <option>1988</option>
                                                    <option>1989</option>
                                                    <option>1990</option>
                                                    <option>1991</option>
                                                    <option>1992</option>
                                                    <option>1993</option>
                                                    <option>1994</option>
                                                    <option>1995</option>
                                                    <option>1996</option>
                                                    <option>1997</option>
                                                    <option>1998</option>
                                                    <option>1999</option>
                                                    <option>2001</option>
                                                    <option>2002</option>
                                                    <option>2003</option>
                                                    <option>2004</option>
                                                    <option>2005</option>
                                                    <option>2006</option>
                                                    <option>2007</option>
                                                    <option>2008</option>
                                                    <option>2009</option>
                                                    <option>2010</option>
                                                    <option>2011</option>
                                                    <option>2012</option>
                                                    <option>2013</option>
                                                    <option>2014</option>
                                                    <option>2015</option>
                                                    <option>2016</option>
                                                    <option>2017</option>
                                                </select>
                                                <label> To:</label>
                                                <select className="form-control" id="sel1" ref="cname3_to">
                                                    <option>1960</option>
                                                    <option>1961</option>
                                                    <option>1962</option>
                                                    <option>1963</option>
                                                    <option>1964</option>
                                                    <option>1965</option>
                                                    <option>1966</option>
                                                    <option>1967</option>
                                                    <option>1968</option>
                                                    <option>1969</option>
                                                    <option>1970</option>
                                                    <option>1971</option>
                                                    <option>1972</option>
                                                    <option>1973</option>
                                                    <option>1974</option>
                                                    <option>1975</option>
                                                    <option>1976</option>
                                                    <option>1977</option>
                                                    <option>1978</option>
                                                    <option>1979</option>
                                                    <option>1980</option>
                                                    <option>1981</option>
                                                    <option>1982</option>
                                                    <option>1983</option>
                                                    <option>1984</option>
                                                    <option>1985</option>
                                                    <option>1986</option>
                                                    <option>1987</option>
                                                    <option>1988</option>
                                                    <option>1989</option>
                                                    <option>1990</option>
                                                    <option>1991</option>
                                                    <option>1992</option>
                                                    <option>1993</option>
                                                    <option>1994</option>
                                                    <option>1995</option>
                                                    <option>1996</option>
                                                    <option>1997</option>
                                                    <option>1998</option>
                                                    <option>1999</option>
                                                    <option>2001</option>
                                                    <option>2002</option>
                                                    <option>2003</option>
                                                    <option>2004</option>
                                                    <option>2005</option>
                                                    <option>2006</option>
                                                    <option>2007</option>
                                                    <option>2008</option>
                                                    <option>2009</option>
                                                    <option>2010</option>
                                                    <option>2011</option>
                                                    <option>2012</option>
                                                    <option>2013</option>
                                                    <option>2014</option>
                                                    <option>2015</option>
                                                    <option>2016</option>
                                                    <option>2017</option>
                                                </select>
                                            </div>
                                            <div className="form-group">
                                                <span className="error">*</span> <label>Expertise:</label>
                                                <input type="text" className="form-control" ref="Expertise" onChange={this.handleExperties}
                                                       placeholder="About electives"/>
                                            </div>
                                            <div className="form-group">
                                                <span className="error">*</span> <label>Personality:</label>
                                                <input type="text" className="form-control" ref="Personality" onChange={this.handlePersonality}
                                                       placeholder="Describe your personality"/>
                                            </div>
                                            <div className="form-group">
                                                <span className="error">*</span> <label>Languages:</label>
                                                <input type="text" className="form-control" ref="Languages" onChange={this.handleLanguages}
                                                       placeholder="Commands on the laguages"/>
                                            </div>
                                            <div className="form-group">
                                                <span className="error">*</span>  <label>Software skills:</label>
                                                <input type="text" className="form-control" ref="Soft_skills" onChange={this.handleSoftware_skills}
                                                       placeholder="About your Software skills"/>
                                            </div>
                                            <div className="form-group">
                                                <span className="error">*</span> <label>Hobbies:</label>
                                                <input type="text" className="form-control" ref="Hobbies" onChange={this.handleHobbies}
                                                       placeholder="About your Hobbies"/>
                                            </div>
                                            <div className="form-group">
                                                <span className="error">*</span>  <label>Links/Certificates:</label>
                                                <input type="text" className="form-control" ref="Links" onChange={this.handleLinks}
                                                       placeholder="About your Links"/>
                                            </div>
                                            <div className="form-group">
                                                <label>Image:</label>
                                                <form onSubmit={this._handleSubmit}>
                                                    <input type="file" onChange={this._handleImageChange} ref="image"/>
                                                    {/*<button type="submit" onClick={this._handleSubmit}>Upload Image</button>*/}
                                                </form>
                                            </div>


                                            {/*<div className="form-group">
                                             <label>Departments:</label>
                                             <select className="form-control" ref="department">
                                             <option>Computer Science</option>
                                             <option>Electrical Engineering</option>
                                             <option>Mechanical Engineering</option>
                                             <option>Software Engineering</option>
                                             <option>Algorithms</option>
                                             <option>Data Structure</option>
                                             </select>
                                             </div>*/}
                                            <button type="submit" className="btn btn-default" data-dismiss="modal"
                                                    disabled={!this.state.Address||!this.state.Contact||!this.state.Email||!this.state.Tel||!this.state.UserName||!this.state.Dob||
                                                    !this.state.Address ||!this.state.objective||!this.state.School_name||!this.state.smajor||!this.state.sFrom||!this.state.sTo||
                                                    !this.state.College_name||!this.state.cmajor||!this.state.cFrom||!this.state.cTo||!this.state.Uni_name||!this.state.umajor||
                                                    !this.state.uFrom||!this.state.uTo||!this.state.Cname1||!this.state.post1||!this.state.Desc1||!this.state.Experties||
                                                    !this.state.Personality||!this.state.Languages||!this.state.Software_skills||!this.state.Hobbies||!this.state.Links}
                                                    onClick={this.handleSubmit.bind(this)}>Submit
                                            </button>
                                        </form>
                                    </div>
                                    <div className="modal-footer">
                                        <button type="button" className="btn btn-default" data-dismiss="modal">Close
                                        </button>
                                    </div>
                                </div>

                            </div>
                        </div>
                        <div className="modal fade modal3" id="myModal2" role="dialog">
                            <div className="modal-dialog modal-xl">

                                <div className="modal-content">
                                    <div className="modal-header">
                                        <button type="button" className="close" data-dismiss="modal">&times;</button>
                                    </div>
                                    <div className="modal-body">
                                        <Fourth1 huzaifa={huzaifa}/>
                                    </div>
                                    <div className="modal-footer">
                                        <button type="button" className="btn btn-default" data-dismiss="modal">Close
                                        </button>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div></div></div>


        )
    }
}
C4.propTypes = {
    tasks: PropTypes.array.isRequired,
    huzaifa:PropTypes.string.isRequired,

};

export default createContainer(() => {
    return {
        tasks: Tasks10.find({}).fetch(),
    };
}, C4);