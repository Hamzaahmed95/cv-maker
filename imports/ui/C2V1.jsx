import React, {Component,PropTypes} from 'react';
import {Meteor} from 'meteor/meteor'
import { createContainer } from 'meteor/react-meteor-data'
import Task from './Task.jsx';
import { Tasks7 } from '../api/tasks7.js';
import Cv from './Cv'
import C2V from './C2V'
import Cv2 from './Cv2'
import Third1 from './Third1'
import {Link} from 'react-router'
import ReactDOM from 'react-dom';
class C2V1 extends Component{

    setTimeout(){
        this.setTimeout(this, 5000)
    }handleSubmit(event){
    event.preventDefault;
    // Find the text field via the React ref
    const name = ReactDOM.findDOMNode(this.refs.contact_name).value.trim();
    const contact = ReactDOM.findDOMNode(this.refs.contact_detail).value.trim();
    const email = ReactDOM.findDOMNode(this.refs.email).value.trim();
    const address = ReactDOM.findDOMNode(this.refs.address).value.trim();
    const motiv = ReactDOM.findDOMNode(this.refs.motivation).value.trim();
    const technicalskills = ReactDOM.findDOMNode(this.refs.skills).value.trim();
    const Strengths = ReactDOM.findDOMNode(this.refs.strength).value.trim();
    const hobby = ReactDOM.findDOMNode(this.refs.hobbies).value.trim();
    const elective = ReactDOM.findDOMNode(this.refs.elective).value.trim();
    const achievments = ReactDOM.findDOMNode(this.refs.achievements).value.trim();
    const image = ReactDOM.findDOMNode(this.refs.pic).value.trim();

    Tasks7.insert({
        name,
        contact,
        email,
        address,
        motiv,
        technicalskills,
        Strengths,
        hobby,
        elective,
        achievments,
        image,

        createdAt: new Date(), // current time
    });
    // Clear form
    ReactDOM.findDOMNode(this.refs.contact_name).value="";
    ReactDOM.findDOMNode(this.refs.contact_detail).value="";
    ReactDOM.findDOMNode(this.refs.email).value="";
    ReactDOM.findDOMNode(this.refs.address).value="";
    ReactDOM.findDOMNode(this.refs.motivation).value="";
    ReactDOM.findDOMNode(this.refs.skills).value="";
    ReactDOM.findDOMNode(this.refs.strength).value="";
    ReactDOM.findDOMNode(this.refs.hobbies).value="";
    ReactDOM.findDOMNode(this.refs.elective).value="";
    ReactDOM.findDOMNode(this.refs.achievements).value="";
    ReactDOM.findDOMNode(this.refs.pic).value="";
}

    renderTasks() {
        return this.props.tasks.map((task) => (
            <C2V key={task._id} task={task}/>
        ));
    }



    /* renderTasks2() {
     return this.props.tasks.map((task) => (
     <Cv2 key={task._id} task={task}/>
     ));
     }*/
    showIt() {
        document.getElementsByClassName("div1").style.visibility = "visible";
        setTimeout("showIt()", 1000); // after 1 sec
    }


    render()
    {
        return (
            <div className="hamza">
                <div className="HamzaPicture">
                    <img className="img-responsive img-rounded" width="350px" height="500px" src ="images/01.png" data-dismiss="modal2"/>
                </div>


                <div className="modal fade" id="myModal" role="dialog">
                    <div className="modal-dialog">

                        <div className="modal-content">
                            <div className="modal-header">
                                <button type="button" className="close" data-dismiss="modal">&times;</button>
                                <h4 className="modal-title">Add your Information</h4>
                            </div>
                            <div className="modal-body">
                                <form>
                                    <div className="form-group">
                                        <label>Name: </label>
                                        <input type="text" className="form-control" ref="contact_name"
                                               placeholder="Enter Name "/>
                                    </div>
                                    <div className="form-group">
                                        <label>Contact No:</label>
                                        <input type="text" className="form-control" ref="contact_detail"
                                               placeholder="Enter contact No"/>
                                    </div>
                                    <div className="form-group">
                                        <label>Enter Email:</label>
                                        <input type="text" className="form-control" ref="email"
                                               placeholder="Enter email"/>
                                    </div>
                                    <div className="form-group">
                                        <label>Enter Address:</label>
                                        <input type="text" className="form-control" ref="address"
                                               placeholder="Enter Address"/>
                                    </div>
                                    <div className="form-group">
                                        <label>Enter Motivation:</label>
                                        <input type="text" className="form-control" ref="motivation"
                                               placeholder="Enter your motivation"/>
                                    </div>
                                    <div className="form-group">
                                        <label>Technical Skills:</label>
                                        <input type="text" className="form-control" ref="skills"
                                               placeholder="Your Technical skills"/>
                                    </div>
                                    <div className="form-group">
                                        <label>Strengths:</label>
                                        <input type="text" className="form-control" ref="strength"
                                               placeholder="Enter your strengths"/>
                                    </div>
                                    <div className="form-group">
                                        <label>Hobbies:</label>
                                        <input type="text" className="form-control" ref="hobbies"
                                               placeholder="Hobbies"/>
                                    </div>
                                    <div className="form-group">
                                        <label>Electives:</label>
                                        <input type="text" className="form-control" ref="elective"
                                               placeholder="About electives"/>
                                    </div>
                                    <div className="form-group">
                                        <label>Achievements:</label>
                                        <input type="text" className="form-control" ref="achievements"
                                               placeholder="About your Achievements"/>
                                    </div>
                                    <div className="form-group">
                                        <label>Image:</label>
                                        <input type="file" ref="pic" accept="image"/>
                                    </div>


                                    <div className="form-group">
                                        <label>Departments:</label>
                                        <select className="form-control" ref="department">
                                            <option>Computer Science</option>
                                            <option>Electrical Engineering</option>
                                            <option>Mechanical Engineering</option>
                                            <option>Software Engineering</option>
                                            <option>Algorithms</option>
                                            <option>Data Structure</option>
                                        </select>
                                    </div>
                                    <button type="submit" className="btn btn-default" data-dismiss="modal"
                                            onClick={this.handleSubmit.bind(this)}>Submit
                                    </button>
                                </form>
                            </div>
                            <div className="modal-footer">
                                <button type="button" className="btn btn-default" data-dismiss="modal">Close
                                </button>
                            </div>
                        </div>

                    </div>
                </div>



                <div className="hamzaInfo">
                    <button type="button" className="btn btn-danger hamzaInfo1" data-toggle="modal" data-target="#myModal">
                        Information
                    </button>
                </div>
                <div className="row hamzaButtons">
                    <div className="col-md-6">
                        <Link to ='/Second'><button type="button" className="btn btn-info"> Show Cv</button></Link>
                    </div>
                    <div className="col-md-6"><a href="https://drive.google.com/file/d/0B8oqeLu-fEXtMzhUVzdUSHc4Sm8/view?usp=sharing"><button type="button" className="btn btn-danger hamzaInfo1"   >Convert as pdf</button></a></div>

                </div>
                <div>

                </div>
            </div>

        )
    }
}
C2V1.propTypes = {
    tasks: PropTypes.array.isRequired,

};

export default createContainer(() => {
    return {
        tasks: Tasks7.find({}).fetch(),
    };
}, C2V1);