import { Router, Route, browserHistory } from 'react-router';
import React from 'react';
import StartingPage from './StartingPage';
import First from './First';
import Third from './Third.jsx';
import Cv from './Cv'
import Cv2 from './Cv2'
import First1 from './First1'
import Layout2V2 from './Layout2V2'
import Layout3_a1 from './Layout3_a1'
import Layout3_a2 from './Layout3_a2'
import Layout3_a3 from './Layout3_a3'
import Layout3_a4 from './Layout3_a4'
import Layout3_a5 from './Layout3_a5'
import Layout3_a6 from './Layout3_a6'
import Second1 from './Second1'
import Third2 from './Third2'
import Fourth1 from './Fourth1'
import Home1 from './Home1'
import Home from './Home.jsx'
import hamza from './hamza'
import hamza1 from './hamza1'
import hamza2 from './hamza2'
import hamza2V2 from './hamza2V2'
import hamza3 from './hamza3'
import hamza4 from './hamza4'
import hamza5 from './hamza5'
import hamza6 from './hamza6'
import hamza7 from './hamza7'
import hamza8 from './hamza8'
import hamza9 from './hamza9'
import hamza10 from './hamza10'
import hamza11 from './hamza11'
import hamza12 from './hamza12'
import hamza13 from './hamza13'
import Second from './second'
import Third1 from './Third1'
import Fourth from './Fourth'
import Fifth from './Fifth'
import Fifth1 from './Fifth1'
import Sixth from './Sixth'
import Sixth1 from './Sixth1'
import App1 from './App1'
import C1 from './C1'
import C2 from './C2'
import C3 from './C3'
import C4 from './C4'
import c5 from './c5'
import c6 from './C6'
import ImageUpload from './ImageUpload'


var ReactRouter = React.createClass ({
    render(){
        return(
            <Router history={browserHistory}>
            <Route path="/" component ={Home} />
                <Route path="/App1" component ={App1} />
                <Route path="/First1" component ={First1} />
                <Route path="/Second1" component ={Second1} />
                <Route path="/Fourth1" component ={Fourth1} />
                <Route path="/Third2" component ={Third2} />
            <Route path="/First" component ={First} />
            <Route path="/Third" component ={Third} />
                <Route path="/hamza" component ={hamza} />
                <Route path="/hamza1" component ={hamza1} />
                <Route path="/hamza2" component ={hamza2} />
                <Route path="/hamza2V2" component ={hamza2V2} />
                <Route path="/Layout2V2" component ={Layout2V2} />
                <Route path="/hamza3" component ={hamza3} />
                <Route path="/hamza4" component ={hamza4} />
                <Route path="/hamza5" component ={hamza5} />
                <Route path="/hamza6" component ={hamza6} />
                <Route path="/hamza7" component ={hamza7} />
                <Route path="/hamza8" component ={hamza8} />
                <Route path="/hamza9" component ={hamza9} />

                <Route path="/hamza10" component ={hamza10} />
                <Route path="/hamza11" component ={hamza11} />
                <Route path="/hamza12" component ={hamza12} />
                <Route path="/hamza13" component ={hamza13} />
                <Route path="/second" component ={Second} />
                <Route path="/C1" component ={C1} />
                <Route path="/C2" component ={C2} />
                <Route path="/C3" component ={C3} />
                <Route path="/C4" component ={C4} />
                <Route path="/C5" component ={c5} />
                <Route path="/C6" component ={c6} />
                <Route path="/Third1" component ={Third1} />
                <Route path="/Fourth" component ={Fourth} />
                <Route path="/Fifth" component ={Fifth} />
                <Route path="/layout3" component ={Layout3_a1} />
                <Route path="/layout1" component ={Layout3_a2} />
                <Route path="/layout2" component ={Layout3_a3} />
                <Route path="/layout4" component ={Layout3_a4} />
                <Route path="/layout5" component ={Layout3_a5} />
                <Route path="/layout6" component ={Layout3_a6} />
                <Route path="/Fifth1" component ={Fifth1} />
                <Route path="/Sixth" component ={Sixth} />
                <Route path="/Sixth1" component ={Sixth1} />
                <Route path="/test" component ={ImageUpload} />
                <Route path="/Home1" component ={Home1} />
</Router>
    )
    }
});

export default ReactRouter;