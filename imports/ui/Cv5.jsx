import {Link} from 'react-router'
import App from './App'
import { Tasks } from '../api/tasks.js';
import React, { Component, PropTypes } from 'react';
import { createContainer } from 'meteor/react-meteor-data';

class Cv5 extends Component{
    render() {
        return (
            <div>
                <div className="wrapper">

                    <aside>
                        <div className="asideGray">
                            <section className="popular-recipes">
                                {console.log("here")}
                                <img className="img-circle" src={this.props.huzaifa}/>
                                <h2 className="contact5">Profile Summary</h2>
                                <a className="contact5" href="">Motivation</a>
                                <a className="contact5" href="">Technical Skilss</a>
                                <a className="contact5" href="">Strength</a>
                                <a className="contact5" href="">Extra-Curricular</a>
                                <a className="contact5" href="">Electives</a>
                                <a className="contact5" href="">Experience</a>
                                <a className="contact5" href="">Achievements</a>
                            </section>
                           
                            <section className="contact-details contact5">
                                <h2 className="contact5">Contact</h2>
                                <p>Email : {this.props.task.email}<br /></p>
                                <p>Address :<br />
                                    {this.props.task.address},Karachi<br />
                                    Pakistan</p>
                            </section></div>
                    </aside>
                    <div className="helloGray">
                        <h1 className="heading1 contact5"><cite>{this.props.task.name}</cite></h1>

                        <br/><h4 className="heading2 contact5">Contact detail:{this.props.task.contact}  /  {this.props.task.email}</h4>
                    </div>
                    <section className="courses">
                        <article>
                            <div className="para1 border5">
                                <h3 className="contact5">Motivation</h3>
                                <p>{this.props.task.motiv}</p></div>
                        </article>
                        <article>
                            <div className="para1 border5">
                                <h3 className="contact5">Technical Skills</h3>
                                <p>{this.props.task.technicalskills}</p></div>
                        </article><article>
                        <div className="para1 border5">
                            <h3 className="contact5">Strength</h3>
                            <ul>
                                <li>{this.props.task.Strengths}</li>

                            </ul></div>
                    </article>
                        <article>
                            <div className="para1 border5">
                                <h3 className="contact5">Electives:</h3>
                                <ul>
                                    <li>{this.props.task.elective}
                                    </li>

                                </ul></div>
                        </article>
                        <article>
                            <div className="para1 border5">
                                <h3 className="contact5">Hobby</h3>
                                <ul>
                                    <li>{this.props.task.hobby}
                                    </li>

                                </ul></div>
                        </article>
                        <article>
                            <div className="para1 border5">
                                <h3 className="contact5">Achievements:</h3>
                                <ul>
                                    <li>{this.props.task.achievments}
                                    </li>

                                </ul></div>
                        </article>

                    </section>

                    <footer className="footerGray contact5">
                        &copy; 2017 {this.props.task.name}
                    </footer>
                </div></div>
        );
    }}

export default  Cv5 ;