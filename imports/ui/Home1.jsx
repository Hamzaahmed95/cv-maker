import React, {Component,PropTypes} from 'react';

import { createContainer } from 'meteor/react-meteor-data'
import Task from './Task.jsx';
import { Tasks } from '../api/tasks.js';
import Cv from './Cv'
import Cv2 from './Cv2'
import {Link} from 'react-router'
import ReactDOM from 'react-dom';
class home extends Component{
    handleSubmit(event){
        event.preventDefault;
        // Find the text field via the React ref
        const name = ReactDOM.findDOMNode(this.refs.contact_name).value.trim();
        const contact = ReactDOM.findDOMNode(this.refs.contact_detail).value.trim();
        const email = ReactDOM.findDOMNode(this.refs.email).value.trim();
        const address = ReactDOM.findDOMNode(this.refs.address).value.trim();
        const motiv = ReactDOM.findDOMNode(this.refs.motivation).value.trim();
        const technicalskills = ReactDOM.findDOMNode(this.refs.skills).value.trim();
        const Strengths = ReactDOM.findDOMNode(this.refs.strength).value.trim();
        const hobby = ReactDOM.findDOMNode(this.refs.hobbies).value.trim();
        const elective = ReactDOM.findDOMNode(this.refs.elective).value.trim();
        const achievments = ReactDOM.findDOMNode(this.refs.achievements).value.trim();
        const image = ReactDOM.findDOMNode(this.refs.pic).value.trim();

        Tasks.insert({
            name,
            contact,
            email,
            address,
            motiv,
            technicalskills,
            Strengths,
            hobby,
            elective,
            achievments,
            image,

            createdAt: new Date(), // current time
        });
        // Clear form
        ReactDOM.findDOMNode(this.refs.contact_name).value="";
        ReactDOM.findDOMNode(this.refs.contact_detail).value="";
        ReactDOM.findDOMNode(this.refs.email).value="";
        ReactDOM.findDOMNode(this.refs.address).value="";
        ReactDOM.findDOMNode(this.refs.motivation).value="";
        ReactDOM.findDOMNode(this.refs.skills).value="";
        ReactDOM.findDOMNode(this.refs.strength).value="";
        ReactDOM.findDOMNode(this.refs.hobbies).value="";
        ReactDOM.findDOMNode(this.refs.elective).value="";
        ReactDOM.findDOMNode(this.refs.achievements).value="";
        ReactDOM.findDOMNode(this.refs.pic).value="";
    }

    renderTasks() {
        return this.props.tasks.map((task) => (
            <Cv key={task._id} task={task}/>
        ));
    }



    renderTasks2() {
        return this.props.tasks.map((task) => (
            <Cv2 key={task._id} task={task}/>
        ));
    }
    showIt() {
        document.getElementsByClassName("div1").style.visibility = "visible";
        setTimeout("showIt()", 1000); // after 1 sec
    }



    render()
    {
        return (
            <div className="text">
                <Link to='/Outsider'><button className="btn btn-danger button3"><strong>Select The Theme</strong></button></Link>

                <button type="button" className="btn btn-info" data-toggle="modal" data-target="#myModal">
                    Information
                </button>
                <div className="modal fade" id="myModal" role="dialog">
                    <div className="modal-dialog">

                        <div className="modal-content">
                            <div className="modal-header">
                                <button type="button" className="close" data-dismiss="modal">&times;</button>
                                <h4 className="modal-title">Add your Information</h4>
                            </div>
                            <div className="modal-body">
                                <form>
                                    <div className="form-group">
                                        <label>Contact Name: </label>
                                        <input type="text" className="form-control" ref="contact_name"
                                               placeholder="Enter Contact"/>
                                    </div>
                                    <div className="form-group">
                                        <label>Contact Detail:</label>
                                        <input type="text" className="form-control" ref="contact_detail"
                                               placeholder="Enter contact detail"/>
                                    </div>
                                    <div className="form-group">
                                        <label>Email:</label>
                                        <input type="text" className="form-control" ref="email"
                                               placeholder="Enter email"/>
                                    </div>
                                    <div className="form-group">
                                        <label>Address:</label>
                                        <input type="text" className="form-control" ref="address"
                                               placeholder="Enter Address"/>
                                    </div>
                                    <div className="form-group">
                                        <label>Motiv:</label>
                                        <input type="text" className="form-control" ref="motivation"
                                               placeholder="Enter your motivation"/>
                                    </div>
                                    <div className="form-group">
                                        <label>Technical Skills:</label>
                                        <input type="text" className="form-control" ref="skills"
                                               placeholder="Your Technical skills"/>
                                    </div>
                                    <div className="form-group">
                                        <label>Strengths:</label>
                                        <input type="text" className="form-control" ref="strength"
                                               placeholder="Enter your strengths"/>
                                    </div>
                                    <div className="form-group">
                                        <label>Hobbies:</label>
                                        <input type="text" className="form-control" ref="hobbies"
                                               placeholder="Hobbies"/>
                                    </div>
                                    <div className="form-group">
                                        <label>Electives:</label>
                                        <input type="text" className="form-control" ref="elective"
                                               placeholder="About electives"/>
                                    </div>
                                    <div className="form-group">
                                        <label>Achievements:</label>
                                        <input type="text" className="form-control" ref="achievements"
                                               placeholder="About your Achievements"/>
                                    </div>
                                    <div className="form-group">
                                        <label>Image:</label>
                                        <input type="file" ref="pic" accept="image"/>
                                    </div>


                                    <div className="form-group">
                                        <label>Departments:</label>
                                        <select className="form-control" ref="department">
                                            <option>Computer Science</option>
                                            <option>Electrical Engineering</option>
                                            <option>Mechanical Engineering</option>
                                            <option>Software Engineering</option>
                                            <option>Algorithms</option>
                                            <option>Data Structure</option>
                                        </select>
                                    </div>
                                    <button type="submit" className="btn btn-default" data-dismiss="modal"
                                            onClick={this.handleSubmit.bind(this)}>Submit
                                    </button>
                                </form>
                            </div>
                            <div className="modal-footer">
                                <button type="button" className="btn btn-default" data-dismiss="modal">Close
                                </button>
                            </div>
                        </div>

                    </div>
                </div>

                <Link to ='/First'><button type="button" className="btn btn-info"   >Cv</button></Link>
                <div>
                    <Link to ='/Third'><button type="button" className="btn btn-info"   >Cv2</button></Link>
                </div>

            </div>
        )
    }
}
home.propTypes = {
    tasks: PropTypes.array.isRequired,
    //count: PropTypes.object.isRequired,
    //mhs: PropTypes.object.isRequired,
    //two: PropTypes.object.isRequired,
};

export default createContainer(() => {
    return {
        tasks: Tasks.find({}).fetch(),

    };
}, home);