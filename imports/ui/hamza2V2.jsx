import React, {Component,PropTypes} from 'react';
import { createContainer } from 'meteor/react-meteor-data'
import { Tasks } from '../api/tasks.js';
import {Link} from 'react-router'
class hamza2V2 extends Component{
    render() {
        return (
            <div className="hamza container-fluid">
                <div className="text-center">
                    <img src="images/cv.png" className="rounded" alt="..." height="10%" width="10%"/>
                </div>
                <div className="text-center">
                    <h4 className="text-center c2">CV MAKER</h4>
                </div>
                <div className="hamza22">
                    <div className="text-center">
                        <p className="text-center c3" height="20%" width="20%">What do you want to make today?</p>
                    </div>
                    <div className="row hamza2row">
                        <div className="col-md-4">
                            <Link to ='/C1'>
                                <div className="img-hover green1 img-rounded">
                                    <img src="images/v26.png" className="img-responsive img-fluid img-thumbnail center-block img-zoom scroll1" height="50%" width="50%"/>
                                    <div className="text-center">
                                        <h2 className="text-center3">Dark Green</h2>
                                    </div>
                                </div>
                            </Link>
                        </div>
                        <div className="col-md-4">
                            <Link to ='/C2'>
                                <div className="img-hover blue1 img-rounded">
                                    <img src="images/v21.png" className="img-responsive img-fluid img-thumbnail center-block img-zoom scroll1" height="50%" width="50%"/>
                                    <div className="text-center">
                                        <h2 className="text-center3">Dark Blue</h2>
                                    </div>
                                </div>
                            </Link>
                        </div>
                        <div className="col-md-4">
                            <Link to ='/C3'>
                                <div className="img-hover purple1 img-rounded">
                                    <img src="images/v22.png" className="img-responsive img-fluid img-thumbnail center-block img-zoom scroll1" height="50%" width="50%"/>
                                    <div className="text-center">
                                        <h2 className="text-center3">Purple</h2>
                                    </div>
                                </div>
                            </Link>

                        </div>
                    </div>
                    <div className="row hamza2row">
                        <div className="col-md-4">
                            <Link to ='/C4'>
                                <div className="img-hover black1 img-rounded">
                                    <img src="images/v23.png" className="img-responsive img-fluid img-thumbnail center-block img-zoom scroll1" height="50%" width="50%"/>
                                    <div className="text-center">
                                        <h2 className="text-center3">Black</h2>
                                    </div>
                                </div>
                            </Link>
                        </div>
                        <div className="col-md-4">
                            <Link to ='/C5'>
                                <div className="img-hover darkred1 img-rounded">
                                    <img src="images/v24.png" className="img-responsive img-fluid img-thumbnail center-block img-zoom scroll1" height="50%" width="50%"/>
                                    <div className="text-center">
                                        <h2 className="text-center3">Maroon</h2>
                                    </div>
                                </div>
                            </Link>
                        </div>
                        <div className="col-md-4">
                            <Link to ='/C6'>
                                <div className="img-hover saddlebrown1 img-rounded">
                                    <img src="images/v25.png" className="img-responsive img-fluid img-thumbnail center-block img-zoom scroll1" height="50%" width="50%"/>
                                    <div className="text-center">
                                        <h2 className="text-center3">Dark Brown</h2>
                                    </div>
                                </div>
                            </Link>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}
hamza2V2.propTypes = {
    tasks: PropTypes.array.isRequired,
};

export default createContainer(() => {
    return {
        tasks: Tasks.find({}).fetch(),
    };
}, hamza2V2);