import {Link} from 'react-router'
import ReactDOM from 'react-dom';
import React, { Component, PropTypes } from 'react';

import { Tasks21 } from '../api/tasks21.js';
import Layout33 from './Layout33'
import Cv2 from './Cv2'
import { createContainer } from 'meteor/react-meteor-data';
class Layout313 extends Component{

    renderTasks(){
        const huzaifa2=this.props.huzaifa;

        return this.props.tasks.map((task) => (

            <Layout33 key={task._id} task={task}  huzaifa={huzaifa2}/>
        ));

    }


    render() {
        return (
            <div className="sixth container">
                <h1>hello</h1>
                {console.log("in render")}
                {this.renderTasks()}
            </div>
        );
    }}


Layout313.propTypes = {
    huzaifa:PropTypes.string.isRequired,
};

export default createContainer(() => {
    return {
        tasks: Tasks21.find({}, { sort: { createdAt: -1 } }).fetch(),


    };
}, Layout313);