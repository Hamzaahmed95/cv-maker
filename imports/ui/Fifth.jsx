import {Link} from 'react-router'
import ReactDOM from 'react-dom';
import React, { Component, PropTypes } from 'react';

import { Tasks5 } from '../api/tasks5.js';
import Cv6 from './Cv6'
import Cv2 from './Cv2'
import { createContainer } from 'meteor/react-meteor-data';
class Fifth extends Component{

    renderTasks(){
        const huzaifa2=this.props.huzaifa;
        return this.props.tasks.map((task) => (
            <Cv6 key={task._id} task={task} huzaifa={huzaifa2} />
        ));

    }


    render() {
        return (
            <div className="sixth container">
                {this.renderTasks()}
            </div>
        );
    }}


Fifth.propTypes = {

    huzaifa:PropTypes.string.isRequired,
};

export default createContainer(() => {
    return {
        tasks: Tasks5.find({}).fetch(),

    };
}, Fifth);