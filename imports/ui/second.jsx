import {Link} from 'react-router'
import ReactDOM from 'react-dom';
import React, { Component, PropTypes } from 'react';

import { Tasks2 } from '../api/tasks2.js';
import Cv3 from './Cv3'
import Cv2 from './Cv2'
import { createContainer } from 'meteor/react-meteor-data';
class Second extends Component{

    renderTasks(){
        const huzaifa2=this.props.huzaifa;
        return this.props.tasks.map((task) => (
            <Cv3 key={task._id} task={task} huzaifa={huzaifa2} />
        ));

    }


    render() {
        return (
            <div className="sixth container">
                {this.renderTasks()}
            </div>
        );
    }}


Second.propTypes = {
    huzaifa:PropTypes.string.isRequired,
};

export default createContainer(() => {
    return {
        tasks: Tasks2.find({}).fetch(),

    };
}, Second);