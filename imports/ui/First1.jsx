import {Link} from 'react-router'
import ReactDOM from 'react-dom';
import React, { Component, PropTypes } from 'react';

import { Tasks7 } from '../api/tasks7.js';
import C2v from './C2V'
import Cv2 from './Cv2'
import { createContainer } from 'meteor/react-meteor-data';
class First1 extends Component{

    renderTasks(){
        const huzaifa2=this.props.huzaifa;
        return this.props.tasks.map((task) => (

            <C2v key={task._id} task={task} huzaifa={huzaifa2}/>

        ));

    }

    render() {
        return (
            <div className="sixth container">
                {this.renderTasks()}
            </div>
        );
    }}


First1.propTypes = {
    huzaifa:PropTypes.string.isRequired,
};

export default createContainer(() => {
    return {
        tasks: Tasks7.find({}, { sort: { createdAt: -1 } }).fetch(),


    };
}, First1);