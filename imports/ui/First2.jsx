import {Link} from 'react-router'
import ReactDOM from 'react-dom';
import React, { Component, PropTypes } from 'react';

import { Tasks13 } from '../api/tasks13.js';
import Cv4 from './Cv4'
import C13 from './C13'
import { createContainer } from 'meteor/react-meteor-data';
class First2 extends Component{

    renderTasks(){

        const huzaifa2=this.props.huzaifa;
        return this.props.tasks.map((task) => (

            <C13 key={task._id} task={task} huzaifa={huzaifa2}/>

        ));

    }


    render() {
        return (
            <div className="sixth container">
                {/*console.log("in render"+this.props.huzaifa)*/}
                {/*<img src={this.props.huzaifa}/>*/}
                {this.renderTasks()}

            </div>
        );
    }}


First2.propTypes = {
    huzaifa:PropTypes.string.isRequired,
};

export default createContainer(() => {
    return {
        tasks: Tasks13.find({}).fetch(),

    };
}, First2);