import {Link} from 'react-router'
import ReactDOM from 'react-dom';
import React, { Component, PropTypes } from 'react';

import { Tasks23 } from '../api/tasks23.js';
import Layout35 from './Layout35'
import Cv2 from './Cv2'
import { createContainer } from 'meteor/react-meteor-data';
class Layout315 extends Component{

    renderTasks(){
        const huzaifa2=this.props.huzaifa;

        return this.props.tasks.map((task) => (

            <Layout35 key={task._id} task={task}  huzaifa={huzaifa2}/>
        ));

    }


    render() {
        return (
            <div className="sixth container">
                <h1>hello</h1>
                {console.log("in render")}
                {this.renderTasks()}
            </div>
        );
    }}


Layout315.propTypes = {
    huzaifa:PropTypes.string.isRequired,
};

export default createContainer(() => {
    return {
        tasks: Tasks23.find({}, { sort: { createdAt: -1 } }).fetch(),


    };
}, Layout315);