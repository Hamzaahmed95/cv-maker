import {Link} from 'react-router'
import ReactDOM from 'react-dom';
import React, { Component, PropTypes } from 'react';

import { Tasks16 } from '../api/tasks16.js';
import Cv4 from './Cv4'
import C16 from './C16'
import { createContainer } from 'meteor/react-meteor-data';
class Fifth2 extends Component{

    renderTasks(){

        const huzaifa2=this.props.huzaifa;
        return this.props.tasks.map((task) => (

            <C16 key={task._id} task={task} huzaifa={huzaifa2}/>

        ));

    }


    render() {
        return (
            <div className="sixth container">
                {/*console.log("in render"+this.props.huzaifa)*/}
                {/*<img src={this.props.huzaifa}/>*/}
                {this.renderTasks()}

            </div>
        );
    }}


Fifth2.propTypes = {
    huzaifa:PropTypes.string.isRequired,
};

export default createContainer(() => {
    return {
        tasks: Tasks16.find({}).fetch(),

    };
}, Fifth2);