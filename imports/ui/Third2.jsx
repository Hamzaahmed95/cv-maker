import {Link} from 'react-router'
import ReactDOM from 'react-dom';
import React, { Component, PropTypes } from 'react';

import { Tasks9 } from '../api/tasks9.js';
import C4V from './C4V'
import Cv2 from './Cv2'
import { createContainer } from 'meteor/react-meteor-data';
class Third2 extends Component{

    renderTasks(){
        const huzaifa2=this.props.huzaifa;

        return this.props.tasks.map((task) => (
            <C4V key={task._id} task={task} huzaifa={huzaifa2} />
        ));

    }


    render() {
        return (
            <div className="sixth container">
                {this.renderTasks()}
            </div>
        );
    }}

Third2.propTypes = {

    huzaifa:PropTypes.string.isRequired,
};

export default createContainer(() => {
    return {
        tasks: Tasks9.find({}, { sort: { createdAt: -1 } }).fetch(),


    };
}, Third2);