import {Link} from 'react-router'
import App from './App'
import { Tasks } from '../api/tasks.js';
import React, { Component, PropTypes } from 'react';
import { createContainer } from 'meteor/react-meteor-data';

class Cv6 extends Component{
    render() {
        return (
            <div>
                <div className="wrapper">

                    <aside>
                        <div className="asideYellow">
                            <section className="popular-recipes">
                                {console.log("here")}
                                <img className="img-circle" src={this.props.huzaifa}/>
                                <h2>Profile Summary</h2>
                                <a className="contact6" href="">Motivation</a>
                                <a className="contact6" href="">Technical Skilss</a>
                                <a className="contact6" href="">Strength</a>
                                <a className="contact6" href="">Extra-Curricular</a>
                                <a className="contact6" href="">Electives</a>
                                <a className="contact6" href="">Experience</a>
                                <a className="contact6" href="">Achievements</a>
                            </section>
                        
                            <section className="contact-details contact6">
                                <h2>Contact</h2>
                                <p>Email : {this.props.task.email}<br /></p>
                                <p>Address :<br />
                                    {this.props.task.address},Karachi<br />
                                    Pakistan</p>
                            </section></div>
                    </aside>
                    <div className="helloYellow">
                        <h1 className="heading1 contact6"><cite>{this.props.task.name}</cite></h1>

                        <br/><h4 className="heading2 contact6">Contact detail:{this.props.task.contact}  /  {this.props.task.email}</h4>
                    </div>
                    <section className="courses">
                        <article>
                            <div className="para1 border6 ">
                                <h3 className="contact6">Motivation</h3>
                                <p>{this.props.task.motiv}</p></div>
                        </article>
                        <article>
                            <div className="para1 border6">
                                <h3 className="contact6">Technical Skills</h3>
                                <p>{this.props.task.technicalskills}</p></div>
                        </article><article>
                        <div className="para1 border6">
                            <h3 className="contact6">Strength</h3>
                            <ul>
                                <li>{this.props.task.Strengths}</li>

                            </ul></div>
                    </article>
                        <article>
                            <div className="para1 border6">
                                <h3 className="contact6">Electives:</h3>
                                <ul>
                                    <li>{this.props.task.elective}
                                    </li>

                                </ul></div>
                        </article>
                        <article>
                            <div className="para1 border6">
                                <h3 className="contact6">Hobby</h3>
                                <ul>
                                    <li>{this.props.task.hobby}
                                    </li>

                                </ul></div>
                        </article>
                        <article>
                            <div className="para1 border6">
                                <h3 className="contact6">Achievements:</h3>
                                <ul>
                                    <li>{this.props.task.achievments}
                                    </li>

                                </ul></div>
                        </article>

                    </section>

                    <footer className="footerYellow">
                        &copy; 2017 {this.props.task.name}
                    </footer>
                </div></div>
        );
    }}

export default  Cv6 ;