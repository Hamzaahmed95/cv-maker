import React, {Component,PropTypes} from 'react';
import { createContainer } from 'meteor/react-meteor-data'
import { Tasks } from '../api/tasks.js';
import {Link} from 'react-router'
class hamza1 extends Component{



    render()
    {
        return (

            <div className="hamza container">

                <div className="hamza23">
                <div className="text-center">
                    <img src="images/cv3.png" className="rounded" alt="..." height="30%" width="30%"/>
                </div>
                <div className="text-center">
                    <h4 className="text-center c12">CV MAKER</h4>
                </div>



                    <div className="row hamza2row">
                        <div className="col-md-4">
                            <Link to ='/hamza2V2'>
                                <div className="img-hover asideB img-rounded"><img src="images/CV2.png" className="img-responsive img-fluid img-thumbnail center-block img-zoom scroll1" height="50%" width="50%"/><div className="text-center">
                                    <h2 className="text-center3">Layout 1</h2>
                                </div></div></Link>
                        </div>
                        <div className="col-md-4">
                            <Link to ='/hamza2'> <div className="img-hover asideP img-rounded"><img src="images/CV1.png" className="img-responsive img-fluid img-thumbnail center-block img-zoom scroll1" height="50%" width="50%"/>   <div className="text-center">
                                <h2 className="text-center3">Layout 2</h2>
                            </div></div></Link>
                        </div>

                        <div className="col-md-4">
                            <Link to ='/Layout2V2'> <div className="img-hover asideY img-rounded"><img src="images/l1.png" className="img-responsive img-fluid img-thumbnail center-block img-zoom scroll1" height="50%" width="50%"/>   <div className="text-center">
                                <h2 className="text-center3">Layout 3</h2>
                            </div></div></Link>
                        </div>
                    </div>
                    <h2 className="text-center h1"><em>"Good Design Is Good Business."</em></h2>
                </div>

            </div>

        )
    }
}
export default createContainer(() => {
    return {
    };
}, hamza1);