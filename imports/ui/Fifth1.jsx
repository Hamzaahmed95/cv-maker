import {Link} from 'react-router'
import ReactDOM from 'react-dom';
import React, { Component, PropTypes } from 'react';

import { Tasks11 } from '../api/tasks11.js';
import C6V from './C6V'
import Cv2 from './Cv2'
import { createContainer } from 'meteor/react-meteor-data';
class Fifth1 extends Component{

    renderTasks(){
        const huzaifa2=this.props.huzaifa;
        return this.props.tasks.map((task) => (

            <C6V key={task._id} task={task} huzaifa={huzaifa2}/>
        ));

    }


    render() {
        return (
            <div className="sixth container">
                {this.renderTasks()}
            </div>
        );
    }}


Fifth1.propTypes = {
    huzaifa:PropTypes.string.isRequired,
};

export default createContainer(() => {
    return {
        tasks: Tasks11.find({}, { sort: { createdAt: -1 } }).fetch(),


    };
}, Fifth1);