import {Link} from 'react-router'
import ReactDOM from 'react-dom';
import React, { Component, PropTypes } from 'react';

import { Tasks3 } from '../api/tasks3.js';
import Cv4 from './Cv4'
import Cv2 from './Cv2'
import { createContainer } from 'meteor/react-meteor-data';
class Third1 extends Component{

    renderTasks(){

        const huzaifa2=this.props.huzaifa;
        return this.props.tasks.map((task) => (

            <Cv4 key={task._id} task={task} huzaifa={huzaifa2}/>

        ));

    }


    render() {
        return (
            <div className="sixth container">
                {/*console.log("in render"+this.props.huzaifa)*/}
                {/*<img src={this.props.huzaifa}/>*/}
                {this.renderTasks()}

            </div>
        );
    }}


Third1.propTypes = {
    huzaifa:PropTypes.string.isRequired,
};

export default createContainer(() => {
    return {
        tasks: Tasks3.find({}).fetch(),

    };
}, Third1);