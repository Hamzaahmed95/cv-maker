import {Link} from 'react-router'
import React, { Component, PropTypes } from 'react';
import { createContainer } from 'meteor/react-meteor-data';
import AccountsUIWrapper from './AccountsUIWrapper.jsx';
import Public2 from './Public2.jsx'
import Public1 from './Public1.jsx'
import Private2 from './Private2.jsx'
class StartingPage extends Component{

    handleSubmit(event) {
        event.preventDefault();

        // Find the text field via the React ref
        const text = ReactDOM.findDOMNode(this.refs.textInput).value.trim();
        const age = ReactDOM.findDOMNode(this.refs.age).value.trim();
        const col = ReactDOM.findDOMNode(this.refs.col).value.trim();
        Tasks.insert({
            text,
            age,
            col,
            createdAt: new Date(), // current time
            owner: Meteor.userId(),           // _id of logged in user
            username: Meteor.user().username,  // username of logged in user
        });
        // Clear form
        ReactDOM.findDOMNode(this.refs.textInput).value = '';
        ReactDOM.findDOMNode(this.refs.age).value = '';

        ReactDOM.findDOMNode(this.refs.col).value = '';
    }

    render() {
    return (
        <div className="container"  >

</div>
        );
}}


StartingPage.propTypes = {
    currentUser: PropTypes.object,};

export default createContainer(() => {
    return {
        currentUser: Meteor.user(),


    };
}, StartingPage);


