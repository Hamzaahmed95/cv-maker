import React, { Component, PropTypes } from 'react';
import { createContainer } from 'meteor/react-meteor-data';
import ReactDOM from 'react-dom';
import { Tasks } from '../api/tasks.js';
import Task from './Task.jsx';

class App extends Component {
    handleSubmit(event) {
        event.preventDefault();

        // Find the text field via the React ref
        const text = ReactDOM.findDOMNode(this.refs.textInput).value.trim();
        const age = ReactDOM.findDOMNode(this.refs.age).value.trim();
        const col = ReactDOM.findDOMNode(this.refs.col).value.trim();
        Tasks.insert({
            text,
            age,
            col,
            createdAt: new Date(), // current time
        });
        // Clear form
        ReactDOM.findDOMNode(this.refs.textInput).value = '';
        ReactDOM.findDOMNode(this.refs.age).value = '';

        ReactDOM.findDOMNode(this.refs.col).value = '';
    }

    renderTasks(){
        return this.props.tasks.map((task) => (
            <Task key={task._id} task={task} />
        ));

    }


    render() {
        return (
            <div className="container">
                <header>
                    <h1>Phone Mangement System</h1>

                <form className="new-task"  >



                    <button type="button" className="btn btn-info" data-toggle="collapse" data-target="#demo">Show/Form</button>

                    <div id="demo">
                                <h1>Adding Contacts</h1>
                            <input
                                    type="text"
                                    ref="textInput"
                                    placeholder="Contact Name"
                                />
                                <input
                                    type="text"
                                    ref="age"
                                    placeholder="Contact Number"
                                />
                                <input
                                    type="text"
                                    ref="col"
                                    placeholder="Sim Name"
                                />

                        <button type="submit" onClick={this.handleSubmit.bind(this)}>Submit</button>


                        </div>

                    <button type="button" className="btn btn-info" data-toggle="collapse" data-target="ufone">Ufone</button>
                    <div id="ufone">
                        <h1>
                        </h1>

                    </div>
                </form>


                </header>
                <ul>
                    {this.renderTasks()}
                </ul>

            </div>
        );
    }

}

App.propTypes = {
    tasks: PropTypes.array.isRequired,
    ufone2: PropTypes.array.isRequired,
};

export default createContainer(() => {
    return {
        tasks: Tasks.find({}, { sort: { createdAt: -1 } }).fetch(),
        ufone2: Tasks.find( { col: "ufone" } ).fetch(),

};
}, App);