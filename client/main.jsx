import React from 'react';
import { Meteor } from 'meteor/meteor';
import { render } from 'react-dom';
import '../imports/startup/account-config.js';
import ReactRouter from '../imports/ui/ReactRouter.jsx';
import StartingPage from '../imports/ui/StartingPage'
import Login from '../imports/ui/Login'
import Cv from '../imports/ui/Cv'

import Cv2 from '../imports/ui/Cv2'
import Third from '../imports/ui/Third'
import First from '../imports/ui/First'

import Home from '../imports/ui/Home'


Meteor.startup(() => {
    render(<ReactRouter/>, document.getElementById('render-target'));
});