#CV-MAKER
>CV-MAKER is a platform for developing online CV. You can upload your information in a simple form and let CV-MAKER do prepare CV for you.

##INSTALLATION: 
#####Steps to be followed:

1.	Install the meteor from below link (if not installed).
	1. For Windows: [Meteor](https://www.meteor.com/install)
	2. For Linux/OSX: curl https://install.meteor.com/ | sh
1.	After completing installation and cloning the project successfully, Go to the project directory and follow the steps below:
	

##SET UP:

	
1. meteor npm install (downloading all the dependencies)
1. meteor (for running the project)
1. Open your web browser and go to http://localhost:3000 to see the app running.

####Enjoy!